# dotfiles

Some dotfiles that I use.

- **.config/emacs/\***: some emacs add-ons (non-ELPA)
- **.config/sway**
- **.config/waybar**
- **.emacs.d.minimal**: my primary emacs configuration
- **.doom.d**: I used this emacs config for a little while before reverting to my own, ancient one
- **bin**: sway and other scripts

If you find any of this useful, I'd appreciated hearing about it: bob dot hepple at gmail dot com

<!-- 
Local Variables:
mode: gfm
markdown-command: "pandoc -s -f gfm --metadata title=README"
eval: (add-hook 'after-save-hook (lambda nil (shell-command (concat markdown-command " README.md > README.html"))) nil 'local)
End:
-->
