(defun calculate-rectangle ()
  "Add up any numbers in a rectangle. Commas are treated as part of the number."
  (interactive)
  (save-window-excursion
    (save-excursion
      (defvar calc-register ?z "Register used for temporary storage by calculate-rectangle.")
      (setq beg (point))
      (setq end (mark))
      (if (< end beg)
	  (progn 
	    (setq beg (mark)) 
	    (setq end (point))))
      (copy-rectangle-to-register calc-register beg end)
      (let ((pop-up-windows t))
	(pop-to-buffer "*calc*"))
      (delete-region (point-min) (point-max))
      (insert-register calc-register)
      (replace-string "," "")
      (shell-command-on-region (point-min) (point-max) "calc" t)
      (message "Sum = %s" (buffer-substring (point-min) (point-max)))
      (kill-region (point-min) (point-max))
      (delete-window)
      (kill-buffer "*calc*"))))

