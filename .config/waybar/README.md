# waybar

![bar image](bar.png)*bar image*

The bar should normally be unobstrusive, maybe even hidden with
SIGUSR1 ($mod+period). Grey is dull enough to not be noticed. Similarly, the blue.
Alarms come up in a blinking fiery orange. At all events, I avoid the
angry fruit salad.

Significantly more information is available in the tooltip - if I want
it, I'll hover over the button.

Clickable buttons are documented in the tooltip.

The ordering and min-width of the buttons is designed to reduce the
'dancing' effect when each button is updated. I don't want it to
distract me unless something's wrong.

The workspace names are updated by the script ../../bin/i3-track-prev-focus

The reload button gives an alarm when one of the waybar scripts or
configs has changed - a reminder to send waybar a SIGUSR2 (or $mod+Shift+c).

The hamburger button is presently linked to 'wofi -drun' but
xfce4-appfinder is another possibility among others.

## Config
- config: waybar configuration
- style.css: stylesheet

## scripts

These run just fine on i3blocks as well as waybar.

- bandwidth: upload and download stats. Click -> L:nethogs.
- btc: crypto monitor. Click -> L:xdg-open https://binance.com (imports btc.config)
- hostname: shows the current hostname. Click -> L:wofi menu for remote desktops (imports hostname.config)
- packages: how many packages need an update (fedora). Click -> dnfdragora or xbps -Su
- vpn: vpn state and country code eg AU=Australia). Click -> L:toggles it on and off. Alarms if vpn is off and torrenting is active. tooltip shows hostname, Country, City, technology. Uses ~/bin/vpn
- weather: local weather. Click -> gnome-weather. toolip shows mina, current, max temperature for the day, wind direction & speed, sunrise & sunset. (imports weather.config)
- scratchpad: displays how many windows are in the scratchpad. L: toggle all to/from scratchpad R: fits any floaters to the screen
- representation: displays an ASCII picture of the container layout on the current w/s

## other buttons

- pulseaudio. Click -> L:pavucontrol, M:equaliser, R:blueman
- cpu. Click -> L:gnome-system-monitor
- RAM. Click -> L:gnome-system-monitor
- Temp. Click -> L:sensors
- Battery. Click -> M:toggle-devices R:gnome-control-center power
- Disk. Click -> L:gnome-disks
- Clock. Click -> R:gnome-disks

<!-- 
Local Variables:
mode: gfm
markdown-command: "pandoc -s -f gfm --metadata title=README"
eval: (add-hook 'after-save-hook (lambda nil (shell-command (concat markdown-command " README.md > README.html"))) nil 'local)
End:
-->