# sway - my config

Here be notes on my **sway** config, much of which applies to my **i3** configs.

Note that many functions use the scripts in the ../../bin directory so
make sure they are on your $PATH before trying this.

![screen image](screen.png)

As you can see, there's little to talk about on my screen dump -
there's the waybar of course, but that's pretty much the only rice. On
**i3**: it's i3blocks.

Other than that, there's emacs and foot (**i3**: alacritty) - tiled so the background is
rarely visible. When it is, on **sway** it looks like this, care of
[wlr-sunclock](https://github.com/sentriz/wlr-sunclock):

![background](background.png)

## Startup

On **sway** I generally start from a console rather than a Display Manager, using
my script **sway-start**

On **i3** the script **i3-startup** is called from **startx**.

Once **sway** is going, it runs **sway-start-apps** which is a bash
script coded to start up my normal session. On the laptop, this is
**emacs** and 2 **foot** windows on W/S 1, **firefox** on W/S 2,
**mythtvfrontend** on W/S 3 and multi-media widgets (**pavucontrol**
and **blueman-manager**) on W/S 4.

It can be tricky to get these started in a script without interfering
with each other, so I wrote a python script
[i3-toolwait](https://gitlab.com/wef/dotfiles/-/blob/master/bin/i3-toolwait)
to serialise the various operations - in memory of the ancient
**xtoolwait** of SunOS days.

There's an interesting script that runs in background:

**i3-track-window-events** subscribes to **sway**'s window focus events
and does several things.

Firstly, it uses a 'mark' to identify the previous window. This enables
**$mod+grave** to switch between the current and the previous
window, which is quite handy. 

Secondly, on **sway** this script toggles transparency so that only
the currently focused window is opaque - giving a useful visual
indication of where the focus is. 

Then, this script renames the current workspace so that **waybar**'s
buttons are more meaningful.

Finally, when **firefox** (and certain other) programs acquire focus,
**Shift+Insert** is bound to a **wtype** function to copy the PRIMARY
selection to the input stream - just like gui programs are supposed
to. I really regret that so many apps have dropped this useful
facility.

## Menu

I've made a _a lot_ of keybindings and it's hard to remember them all,
so I wrote
[i3-menu](https://gitlab.com/wef/dotfiles/-/blob/master/bin/i3-menu)
which provides a menu for **sway** and **i3**.

This script requires that bindings in the configuration file is
annotated with comments such as:

```
#### _Window Move window down
    bindsym  $mod+$s+Down  move down
```

This is picked up by the python script which is bound to
**$mod+Shift+Menu** and which displays a menu of commands such as:

![i3-menu](i3-menu.png)
![i3-menu](i3-menu-keystroke.png)

Easy-peasy.

Reverse lookup is also available using the keyboard button - type a
key-chord into the box and it tells you its function.

## Drop-down terminal

For casual use, it's nice to use a drop-down terminal. For this I run:

```
myterm --dropdown
```

with a rule:

```
for_window [app_id="dropdown-term"] floating enable, $slim_border
# i3:
for_window [class="dropdown-term"] floating enable, $slim_border
```

I also run it via my script
[i3-focus](https://gitlab.com/wef/dotfiles/-/blob/master/bin/i3-focus)
to position it at the origin (I don't want to use a rule for that) but
also to pull/push it to the scratchpad if it's already running. This
gives a fast starting terminal.

## Configuration

I tend to use **wofi** rather than **dmenu** (in **i3** it's**rofi**).

## Key bindings - bindsym

Fairly close to the 'canonical' /etc/sway/config. Although ...

I prefer that all unmodified and **Alt+**, **Control+**,
**Shift+Alt+**, **Shift+Control+** key bindings belong to the
applications and are not consumed by **sway**/**i3**. The two
exceptions are the well-known sequences **Alt+Tab** which is serviced
by a bespoke python script
[sway-select-window](https://gitlab.com/wef/dotfiles/-/blob/master/bin/sway-select-window),
(**i3**: rofi)
and **Control+Escape** which runs gnome-system-monitor

**Mod4** (the logo key) is the primary modifier for WM operations.

All WM operations use **Mod4+**

For example:

To move the focus to the window at the left:

```
bindsym $mod+Left  focus left
```

### headless

To run a remote headless **sway** session:

```
sway --config ~/.config/sway/headless
```

To connect to it:

```
vncviewer <SERVER_NAME>::5900
```

Sadly **vncviewer** is still the most performant VNC viewer.
"Sadly" because it uses **Xwayland**.

### exec

Most of the 'plain' keys (like 'a', 'b') have a $mod+a bindsym to run
'something'. These are prime candidates for your own customisation. I have these:

* $mod+b mybluetooth (a menu to switch bluetooth speakers)
* $mod+c clock
* $mod+d program launcher
* $mod+e emacs
* $mod+f full-screen
* $mod+Shift+f firefox (as $mod+f is 'full screen')
* $mod+g gdcalc (an HP-type calculator)
* $mod+h horizontal split
* $mod+l mylock (a menu to govern locking - **sway** only)
* $mod+m move mode
* $mod+o output mode
* $mod+p keepassxc
* $mod+q libreoffice
* $mod+r resize mode
* $mod+s swap mode
* $mod+v vertical split
* $mod+z veracrypt

### misc

I like to have a bright 'indicator' (the thin line at the edge of a
window showing where it would split) as I can't see the default colours.

### modes

On **sway** each 'mode' has a transparent nwg-wrapper help that pops
up while the mode is in effect. This is done by the script
**i3-mode**

![resize mode help screen](mode.png)

#### Resize

$mod+r = 'Resize mode' has the usual `left`/`right`/`up`/`down`
functions plus s(mall), m(edium) and l(arge) and 2, 3, 4, 6, 8 and 9
which resize a floater to 1/2, 1/3, 1/4 ./etc of the screen.

#### Swap

$mod+s is swap mode - not in the default config. `left`/`right`/`up`/`down`
swaps the focus window with the one in that direction.

#### Move

$mod+m moves windows around - `left`/`right`/`up`/`down` as you would expect.
h,j,k and l are like the **vi** motion keys.

These keys reflect the position on the keyboard:

```
    w e r
    s d f
    x c v
```

ie top-left, top-centre, top-right etc

&gt; and &lt; take the window to the next/prev empty workspace as do n and p

#### Move to Output

I rarely use an external monitor, but this mode allows operations to different outputs.

`left`, `right`, `up` and `down` changes the focus to another output. The
shifted version takes the focused window with you.

`PgUp`, `PgDn`, `Home` and `End` move the entire workspace to the appropriate output.

#### Passthrough

This is mainly for use with remote VNC sessions - when a remote
session starts, I usually make it fullscreen and then press $mod+Mute
so that all keystrokes apply to the remote.

## waybar

See [waybar](https://gitlab.com/wef/dotfiles/-/tree/master/.config/waybar)


<!-- 
Local Variables:
mode: gfm
markdown-command: "pandoc -s -f gfm --metadata title=README"
eval: (add-hook 'after-save-hook (lambda nil (shell-command (concat markdown-command " README.md > README.html"))) nil 'local)
End:
-->
