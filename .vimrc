set ignorecase
set nowrapscan
set wrap
set tabstop=4
set softtabstop=4
set expandtab           " tabs are spaces
set showmatch           " highlight matching [{()}]
set incsearch           " search as characters are entered
set hlsearch            " highlight matches
set shiftwidth=8
set noautoindent

map [1~ 0
map [7~ 0
map [4~ $
map [8~ $
map [5~ 
map [6~ 

map Od B
map Oc W
map Ob }
map Oa {
map  !}fmt

map! Od Bi
map! Oc Wi
map! Ob }a
map! Oa {i

runtime ftplugin/man.vim
set bg=dark
set mouse=a
source /etc/vimrc
