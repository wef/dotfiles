;; navigation in this file:
;; Use TAB / S-TAB to cycle visibility (outline-minor-mode) on header lines
;; Use <f1> to cycle visibility in body
;;; todo
;;; prelims
(defmacro disable (&rest body)
  "Ignores BODY and returns nil."
  nil)

;; The native compiler throws up far too many warnings and these can disable mini-buffer use!!!
(setq warning-minimum-level ':error)

;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Bob Hepple"
      user-mail-address "bob.hepple@gmail.com")
;;;; prep for org mode
;; These must be set before org loads:
(setq org-directory "~/Sync/")
(setq org-replace-disputed-keys t)
(setq org-disputed-keys '(([(shift right)]              . [(meta +)]) ;; org-shiftright
                          ([(shift left)]               . [(meta -)]) ;; org-shiftleft
                          ([(shift up)]                 . [(meta p)]) ;; org-shiftup
                          ([(shift down)]               . [(meta n)]) ;; org-shiftdown
                          ([(shift right)]              . [(meta shift +)]) ;; org-shiftcontrolright
                          ([(control shift left)]       . [(meta shift -)]) ;; org-shiftcontrolleft
                          ([(control shift up)]         . [(meta shift \))]) ;; org-shiftcontrolup
                          ([(control shift down)]       . [(meta shift \()]) ;; org-shiftcontroldown
;                          ([(control meta right)]      . [( nil )]) ;; not in org
;                          ([(control meta left)]       . [( nil )]) ;; not in org
;                          ([(control meta up)]         . [( nil )]) ;; not in org
;                          ([(control meta down)]       . [( nil )]) ;; not in org
                          ([(control shift meta right)] . [( nil )]) ;; org-increase-number-at-point
                          ([(control shift meta left)]  . [( nil )]) ;; org-decrease-number-at-point; does not work!
;                          ([(control shift meta up)]   . [( nil )]) ;; not in org
;                          ([(control shift meta down)] . [( nil )]) ;; not in org
                          ([(control comma)]            . [( nil )]) ;; org-cycle-agenda-files
                          ([(control quote)]            . [(kbd "C-\"")]))) ;; org-cycle-agenda-files

;;;; online?
(require 'cl)
(defun online? ()
  "Detect whether you are online (from ohai)."
  (if (and (functionp 'network-interface-list)
           (network-interface-list))
      (cl-some (lambda (iface) (unless (equal "lo" (car iface))
                         (member 'up (first (last (network-interface-info
                                                   (car iface)))))))
               (network-interface-list))
    t))

;;; packages
;;;; initialise package system
(setq package-user-dir (concat user-emacs-directory "/elpa"))
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)
(when (online?)
  (unless package-archive-contents (package-refresh-contents)))
(setq use-package-always-ensure t)
;; (setq use-package-compute-statistics t) ; for use with M-x use-package-report

;;;; load packages
;;;;; ripgrep
(use-package ripgrep
  :defer t
  :config
  (setq ripgrep-arguments '("--follow"))
  (grep-apply-setting 'grep-find-command
                      '("rg --follow -n -H --no-heading --hidden -e '' ." . 36)))
;; this variant searches from the top level if in a git directory:
;;                      '("rg -n -H --no-heading -e '' $(git rev-parse --show-toplevel 2>/dev/null || pwd)" . 27)))
;;;;; whitespace
(use-package whitespace
  ;; :defer t fails to load
  :config
  (whitespace-mode -1))
(setq bh/whitespace-mode "off")
;;;;; helpful
;; from doom:
(use-package helpful
  ;; a better *help* buffer
  ;; :defer t fails to load - although :commands implies :defer!!
  :commands helpful--read-symbol
  :init
  (global-set-key [remap describe-function] #'helpful-callable)
  (global-set-key [remap describe-command]  #'helpful-command)
  (global-set-key [remap describe-variable] #'helpful-variable)
  (global-set-key [remap describe-key]      #'helpful-key)
  (global-set-key [remap describe-symbol]   #'helpful-symbol)
  (global-set-key (kbd "C-c C-d") #'helpful-at-point))
;;;;; desktop
(use-package desktop
  ;; :defer t fails to load
  :config
  (desktop-save-mode 1))
;(desktop-read) ... not actually needed - in fact it raises an error thinking desktop is already running!

;;;;; magit
(use-package magit
  :defer t
  :init
  (setq magit-define-global-key-bindings 'recommended))

;;;;; vc
(use-package vc :defer t)
;;;;; browse-kill-ring
;; replaced by consult-yank-from-kill-ring
;; (use-package browse-kill-ring
;;   :defer t
;;   :config
;;   (browse-kill-ring-default-keybindings))

;;;;; discover-my-major
;; Get an instant cheat sheet for your current major mode
;; with C-h C-m.
(use-package discover-my-major
  :defer t
  :commands (discover-my-major discover-my-mode)
  :bind ("C-h C-m" . discover-my-major))

;;;;; which-key
(use-package which-key
  :defer t
  :commands which-key-mode
  :demand t
  :config
  (which-key-mode)
  ;; Set the delay before which-key appears.
  (setq-default which-key-idle-delay 1.0)
  ;; which-key will truncate special keys by default, eg. SPC turns into
  ;; an orange D. Turn this off to avoid confusion.
  (setq-default which-key-special-keys nil)
  ;; Hit C-h C-k to have which-key show you all top level key bindings.
  :bind ("C-h C-k" . which-key-show-top-level)
  :diminish which-key-mode)

;;;;; ibuffer-vc
(use-package ibuffer-vc :defer t) ; for gk-ibuffer.el
;; better ibuffer format:
(load-file "~/.config/emacs/gk-ibuffer.el")

;;;;; rpm-spec-mode
;; don't use the one in elpa - it is abandoned. It's forked here: https://github.com/vmfhrmfoaj/rpm-spec-mode
;; (use-package rpm-spec-mode
;;   :defer t
;;   :init
;;   (setq auto-mode-alist (append '(("\\.spec" . rpm-spec-mode))
;;                                 auto-mode-alist)))
(require 'rpm-spec-mode)
(setq auto-mode-alist (append '(("\\.spec" . rpm-spec-mode)) auto-mode-alist))

;;;;; esup
(use-package esup
  :ensure t
  ;; To use MELPA Stable use ":pin melpa-stable",
  :pin melpa
  :commands (esup))
;;;;; neotree
;; (defun bh/view-file-in-popper ()
;;   (interactive)
;;   (neotree-quick-look)
;;   (save-excursion
;;     (other-window 1)
;;     (read-only-mode)
;;     (popper-lower-to-popup)
;;     (keymap-local-set (kbd "q") 'kill-buffer-and-window)))
;; (use-package neotree
;;   :config
;;   (setq neo-smart-open t)
;;   :bind (:map neotree-mode-map
;;               ("v" . bh/view-file-in-popper)))
;; (defvar allow-follow t) ; naming is hard - at least we're single tasking!
;; (defun bh/neotree-follow-current-file (&optional foo bar)
;;   "Change Neotree root directory to match current file."
;;   (when (and (neo-global--window-exists-p) buffer-file-name allow-follow)
;;     (setq allow-follow nil)
;;     (save-excursion
;;       (let ((dir (file-name-directory (buffer-file-name))))
;;         (neo-buffer--change-root dir)
;;         (setq allow-follow t))))
;; (advice-add 'switch-to-buffer :after #'bh/neotree-follow-current-file)
;; (advice-add 'find-file        :after #'bh/neotree-follow-current-file)
;; ;(advice-add 'select-window    :after #'bh/neotree-allow-follow) ; weird things happen
;;;;; elfeed

(use-package elfeed
  :defer t
  :bind (:map elfeed-search-mode-map
              ("s" . elfeed-search-set-filter)
              ("S" . elfeed-search-live-filter)
              (";" . elfeed-search-quit-window)
              ("/" . elfeed-search-browse-url)
              ("." . elfeed-search-untag-all-unread)
              ("x" . elfeed-search-browse-url)
              ("z" . elfeed-search-untag-all-unread))
  :config
  (setq elfeed-search-title-max-width 90)
  (setq elfeed-search-trailing-width 10)
  (setq elfeed-use-curl nil)
  (setf elfeed-sort-order 'ascending))

(use-package elfeed-dashboard
  :ensure t
  :bind ("C-c e" . 'elfeed-dashboard)
  :config
  (setq elfeed-dashboard-file "~/.config/emacs/elfeed-dashboard.org")
  ;; update feed counts on elfeed-quit
  (advice-add 'elfeed-search-quit-window :after #'elfeed-dashboard-update-links))

(setq elfeed-feeds '(
                     ("http://xkcd.com/rss.xml" comic xkcd)
                     ("http://rss.slashdot.org/slashdot/slashdotmain" slashdot)
                     ("https://www.reddit.com/r/emacs/new/.rss?sort=new" reddit emacs)
                     ("https://www.reddit.com/r/linux/new/.rss?sort=new" reddit linux)
                     ("https://www.reddit.com/r/voidlinux/new/.rss?sort=new" reddit void)
                     ("https://voidlinux.org/atom.xml" void voidnews)
                     ("https://voidforums.com/feed" void voidforum)
                     ("https://distrowatch.com/news/dw.xml" news distrowatch linux)
                     ("http://oglaf.com/feeds/rss/" comic oglaf)
                     ("https://www.reddit.com/r/linuxquestions/new/.rss?sort=new" reddit linuxquestions)
                     ("https://feeds.bbci.co.uk/news/rss.xml" news bbc)
                     ("https://www.rt.com/rss/" news rt)
                     ("https://hnrss.org/frontpage?count=100" news hacker)
                     ("https://www.reddit.com/r/swaywm/new/.rss?sort=new" reddit sway)
                     ("https://lwn.net/headlines/rss" news lwn.net)
                     ("https://lobste.rs/t/linux.rss" news lobsterlinux linux)
                     ("https://www.emacswiki.org/emacs?action=rss" news emacswiki emacs)
                     ("https://feeds.arstechnica.com/arstechnica/index" news arstechnica)
                     ("https://lemmy.ml/feeds/c/emacs.xml" emacs lemmy)
                     ("https://lemmy.ml/feeds/c/linux.xml" linux lemmy)
                     ("https://lemmy.ml/feeds/c/swaywm.xml" sway lemmy)
                     ("https://liliputing.com/feed/" news liliputing)))

;;(use-package elfeed-goodies) ; nah! messes up pgtk-28.0.50 keyboard input. fixme

;; download ap rss feeds to a local file before running elfeed-dashboard-update:
;; (advice-add 'elfeed-dashboard-update
;;             :before
;;             (lambda ()
;;               (shell-command "ratt https://apnews.com/hub/world-news > ~/tmp/apnews 2>/dev/null")))

;; from: https://github.com/skeeto/elfeed/issues/392
(defun sk/elfeed-db-remove-entry (id)
  "Removes the entry for ID"
  (avl-tree-delete elfeed-db-index id)
  (remhash id elfeed-db-entries))
(defun sk/elfeed-search-remove-selected ()
  "Remove selected entries from database"
  (interactive)
  (let* ((entries (elfeed-search-selected))
         (count (length entries)))
    (when (y-or-n-p (format "Delete %d entries?" count))
      (cl-loop for entry in entries
               do (sk/elfeed-db-remove-entry (elfeed-entry-id entry)))))
  (elfeed-search-update--force))

(defun bh/elfeed-fix-entry-at-parse (type xml entry)
  "It seems :link can only be adjusted here, not in elfeed-new-entry-hook"
  (let ((feed (elfeed-entry-feed-id entry)))
    (cond ((string-match-p "feeds\\.bbci\\." feed)
           ;; bbc started adding #<n> to the guid resulting in
           ;; duplicates in the feed - just remove them
           (when-let ((id (elfeed-entry-id entry))
                      (regex "\\(https://www\\.bbc\\.[^#]+\\)#.+"))
             (when (string-match regex (cdr id))
               (setq new-id (replace-match "\\1" nil nil (cdr id)))
               (setf (cdr id) new-id)
               (elfeed-tag entry 'bh-fixed)))
           (when (string-match-p "/sport/" (elfeed-entry-link entry))
             ;; mark bbc sport as read
             (elfeed-untag entry 'unread)
             (elfeed-tag entry 'bh-fixed)))
          ;; ((string-match-p "rt\\.com" feed)
          ;;  (when (string-match-p "/sport/" (elfeed-entry-link entry))
          ;;    ;; mark rt sport as read - rt doesn't seem to do sport now
          ;;    (elfeed-untag entry 'unread)
          ;;    (elfeed-tag entry 'bh-fixed)))
          ((string-match-p ".*lemmy.*" feed)
           ;; lemmy links to subject matter instead of the discussion (so does hacker news but the discussion links are less interesting and available)
           ;; - revert that:
           (let ((link (elfeed-entry-link entry)))
             (unless (string-match-p "https://lemmy\\.ml/feeds/c/" link)
               (setf (elfeed-entry-link entry) (cdr (elfeed-entry-id entry)))
               (elfeed-tag entry 'bh-fixed)))))))
(add-hook 'elfeed-new-entry-parse-hook #'bh/elfeed-fix-entry-at-parse)

(defadvice elfeed-search-browse-url (around bh/check-elfeed-search-browse-url activate)
  "Check if number of entries to open is excessive before browsing."
  (let* ((entries (elfeed-search-selected))
         (count (length entries)))
    (if (or (< count 4)  ; Adjust threshold as needed
            (y-or-n-p (format-message "Open %d entries in browser?" count)))
        ad-do-it)))

;;;;; eglot
;; see https://www.reddit.com/r/emacs/comments/11oikwv/problems_with_eglot_configuration/
(use-package eglot
  :defer t)
(add-hook 'c-mode-hook #'eglot-ensure)
(add-hook 'c++-mode-hook #'eglot-ensure)
;; (add-hook 'python-mode-hook #'eglot-ensure)

;;;;; json
(use-package json-navigator :ensure t)
;; (use-package jsonrpc :ensure t)
;;;;; eldoc
(use-package eldoc
  :ensure t
  :custom
  (eldoc-echo-area-use-multiline-p t)
  :bind (:map eglot-mode-map
              ("C-c d" . eldoc)
              ("C-c a" . eglot-code-actions)
              ("C-c f" . flymake-show-buffer-diagnostics)
              ("C-c r" . eglot-rename)
              ("C-c <tab>" . corfu-complete)))

;;;;; imenu-list
;; (use-package imenu-list
;;  :bind
;;  ("C-'" . 'imenu-list-smart-toggle)
;;  :config
;;  (setq imenu-list-position 'left)
;;  (add-hook 'python-mode-hook #'(lambda() (imenu-list-minor-mode 1)))
;;  (add-hook 'c-mode-hook #'(lambda() (imenu-list-minor-mode 1)))
;;  (add-hook 'c++-mode-hook #'(lambda() (imenu-list-minor-mode 1))))
;;;;; speedbar
(speedbar -1)
(setq speedbar-show-unknown-files t)
(setq speedbar-directory-unshown-regexp "^\\(\\.*\\)\\'")
;;;;; sr-speedbar
;; NB this has a curious interaction with helpful - when you
;; quit-window from helpful, the window is not zapped so you end up
;; with multiple windows. speedbar is OK.
;(use-package sr-speedbar)
;  :bind
;  ("s-s" . 'sr-speedbar-toggle))
;;;;; bookmark
;; save bookmarks after every change (why is this not the default?!!)
(setq bookmark-save-flag 1)
;;;;; projectile
;; I'm not using projectile ATM but just in cas I change my mind, this was useful:
;; to make C-c p p report a sorted list of projects:
;; (defun bh/projectile-sort-projects (projects)
;;   "Return the sorted list of projects that `projectile-relevant-known-projects'
;; returned."
;;   (sort projects 'string<))
;; (advice-add 'projectile-relevant-known-projects :filter-return #'bh/projectile-sort-projects)
;;;;; key-chords
(use-package key-chord
  :defer t
  :config (key-chord-mode 1))
;;
;; and some chords: at least check /usr/share/dict/words
;;

(key-chord-define-global ";'"     'delete-other-windows) ;; was bh/kill-next-window) ;; C-x 1
;; (key-chord-define-global ",."  'kmacro-end-and-call-macro) ;; C-x e
(key-chord-define-global "qq"     'speedbar)
;;(key-chord-define-global "qq"     #'(lambda () (interactive) (dired ".")))
(key-chord-define-global "ww"     'neotree-find)
(key-chord-define-global "zx"     'bh/buffer-menu)
(key-chord-define-global "xc"     'ibuffer)
(key-chord-define-global "xx"     'delete-other-windows)
;(key-chord-define-global "DD"     'bh/kill-whole-line) ; 'dd' is too common!
;(key-chord-define-global "yy"     'bh/yank-whole-line) ; 'YY' is too much of a stretch
;(key-chord-define-global "CC"     'bh/copy-whole-line) ; 'cc' is too common!

;; these are just too annoying:
;; (key-chord-define-global ",."     "<>\C-b")
;; (key-chord-define-global "''"     "''\C-b")
;; (key-chord-define-global "\"\""   "\"\"\C-b")
;; (key-chord-define-global "<>"     "<>\C-b")
;; (key-chord-qdefine-global "[]"     "[]\C-b")
;; (key-chord-define-global "{}"     "{}\C-b")
;; (key-chord-define-global "()"     "()\C-b")
;; I never use these in practice:
;; (key-chord-define-global "hk"     'describe-key)
;; (key-chord-define-global "hf"     'describe-function)
;; (key-chord-define-global "hv"     'describe-variable)

;;;;; org-mode
;;;;;; org-capture
(setq org-capture-templates
      (quote (("c" "addr" entry (file "~/tmp/addr.org")
"* %?
:PROPERTIES:
:POSITION:
:COMPANY:
:CODE:
:ADDR1:
:ADDR2:
:ADDR3:
:ADDR4:
:TEL:
:FAX:
:XMAS:
:EMAIL:
:NOTES:
:PRINTFLAG:
:SALUTATION:
:DATESTAMP:
:MOBILE:
:END:
"))))

;;;;;; org-mode-hook
(add-hook 'org-mode-hook
	  (lambda ()
            (setq org-ellipsis "\u2935")
            (org-indent-mode 1)
            ;; fixup org-mode's trashing of case-fold-search:
            (setq case-fold-search t)))
;;;;; hideshow mode
;; from /usr/share/emacs/29.0.50/lisp/progmodes/hideshow.el.gz:
(defvar my-hs-hide nil "Current state of hideshow for toggling all.")
(defun my-toggle-hideshow-all () "Toggle hideshow all."
  (interactive)
  (setq my-hs-hide (not my-hs-hide))
  (if my-hs-hide
      (hs-hide-all)
    (hs-show-all)))

;; https://lists.gnu.org/archive/html/emacs-devel/2011-04/msg00562.html
;; called once on a line that contains a hidden block, shows the
;; block; otherwise calls the default action of TAB; called twice on a
;; line that does not contain a hidden block, hide the block from the
;; current position of the cursor
;; (defun tab-hs-hide ( &optional arg )
;;   (interactive "P")
;;   (let ((sl (save-excursion (move-beginning-of-line nil) (point) ) )
;;         (el (save-excursion (move-end-of-line nil) (point) ) )
;;         obj)
;;     (catch 'stop
;;       (dotimes (i (- el sl))
;;         (mapc
;;          (lambda (overlay)
;;            (when (eq 'hs (overlay-get overlay 'invisible))
;;              (setq obj t)))
;;          (overlays-at (+ i sl)))
;;         (and obj (throw 'stop 'stop) ) ) )
;;     (cond ((and (null obj)
;;                 (eq last-command this-command) )
;;            (hs-hide-block) )
;;           (obj
;;            (progn
;;              (move-beginning-of-line nil)
;;              (hs-show-block) ) )
;;           (t
;;            (save-excursion
;;              (funcall (lookup-key (current-global-map) (kbd "^I") ) arg ) ) ) )
;;     ) )

;; modified to work with yasnippet (by chatGPT)
(defun tab-hs-hide (&optional arg)
  "Hide or show code blocks using `hs-minor-mode'.
If no block is found, pass the TAB key to the default action."
  (interactive "P")
  (let ((sl (save-excursion (move-beginning-of-line nil) (point)))
        (el (save-excursion (move-end-of-line nil) (point)))
        (obj nil))
    (catch 'stop
      (dotimes (i (- el sl))
        (mapc
         (lambda (overlay)
           (when (eq 'hs (overlay-get overlay 'invisible))
             (setq obj t)))
         (overlays-at (+ i sl)))
        (and obj (throw 'stop 'stop))))
    (cond ((and (null obj)
                (eq last-command this-command))
           (hs-hide-block))
          (obj
           (progn
             (move-beginning-of-line nil)
             (hs-show-block)))
          (t
           ;; Check if yasnippet should be triggered
           (if (bound-and-true-p yas-minor-mode)
               (call-interactively 'yas-expand)
             ;; If not, perform the default action for TAB
             (funcall (key-binding (kbd "TAB")) arg))))))

(add-hook 'hs-minor-mode-hook
          (lambda ()
            (define-key hs-minor-mode-map (kbd "TAB") 'tab-hs-hide )
            (define-key hs-minor-mode-map [backtab] 'my-toggle-hideshow-all)))

;;;;; outline-minor-mode

;; Make outline-minor-mode operate a bit more like org-mode <tab> &
;; S-<tab>. org-mode has lots of baggage eg many intrusive keymaps. If
;; text-folding is all you need then outline is much simpler eg 1593
;; LOC vs 127834 LOC for org (excl blanks, comments)

(defun bh/outline-show-heading-path ()
  "Display the full path of the headings containing point in the echo area."
  (interactive)
  (let ((path (bh/outline-get-heading-path)))
    (if path
        (message "Heading Path: %s" (mapconcat 'identity path "/"))
      (message "Not in an outline"))))

(defun bh/outline-get-heading-path ()
  "Get the full path of the heading at point."
  (save-excursion
    (let ((path '())
          (heading (outline-back-to-heading)))
      (while heading
        (let ((sub-heading (buffer-substring-no-properties (line-beginning-position) (line-end-position))))
          (setq sub-heading (replace-regexp-in-string (concat "^" outline-regexp) "" sub-heading))
          (setq path (cons sub-heading path))
          (setq heading (ignore-errors (progn (outline-up-heading 1) (outline-back-to-heading))))))
      path)))

(with-eval-after-load "outline"
  (define-key outline-minor-mode-map (kbd "C-c C-c")
              ;; this enables C-c C-c to be used instead of the very awkward C-c C-@ prefix:
              (lookup-key outline-minor-mode-map (kbd "C-c @"))))

(add-hook 'outline-minor-mode-hook
          (lambda ()
            (define-key outline-minor-mode-map [backtab] 'outline-cycle-buffer)
            (define-key outline-minor-mode-map (kbd "C-c C-n") 'outline-next-visible-heading)
            (define-key outline-minor-mode-map (kbd "C-c C-p") 'outline-previous-visible-heading)
            (define-key outline-minor-mode-map (kbd "C-c C-f") 'outline-forward-same-level)
            (define-key outline-minor-mode-map (kbd "C-c C-b") 'outline-backward-same-level)
            (define-key outline-minor-mode-map (kbd "C-c C-u") 'outline-up-heading)
            (define-key outline-minor-mode-map (kbd "C-c C-a") 'outline-show-all)
            (define-key outline-minor-mode-map (kbd "C-c ?")   'bh/outline-show-heading-path)
            (define-key outline-minor-mode-map (kbd "C-c C-c C-a") 'outline-show-all)
            (define-key outline-minor-mode-map (kbd "<f1>") 'outline-toggle-children)

            (setq-local outline-minor-mode-use-buttons 'in-margins)
            (setq-local outline-minor-mode-highlight 'append)
            (setq-local outline-minor-mode-cycle t)))

;;;;; company-mode
;; (use-package company
;;   :ensure t
;; ;;  :after lsp-mode
;; ;;  :hook (lsp-mode . company-mode)
;;   :custom
;;   (company-minimum-prefix-length 3)
;;   (company-idle-delay 3))
;; (add-hook 'after-init-hook 'global-company-mode)
;;;;; lsp-mode (with pylsp (fedora pkg) or pyright (npm package))
;; https://emacs-lsp.github.io/lsp-mode/page/performance/
;; (setq read-process-output-max (* 1024 1024)) ;; 1mb
;; (setq gc-cons-threshold 100000000)
;;
;; (use-package lsp-mode
;;   :ensure t
;;   :commands (lsp lsp-deferred)
;;   :init
;;   (setq lsp-keymap-prefix "C-c l")
;;   :custom
;;   (lsp-enable-which-key-integration t)
;;   (lsp-headerline-breadcrumb-enable t)
;;   (lsp-enable-indentation nil)
;;   (lsp-enable-on-type-formatting nil)
;;   (lsp-modeline-code-actions-enable nil)
;;   (lsp-modeline-diagnostics-enable t)
;;   (lsp-clients-clangd-args '("--header-insertion=never")))
;; (use-package lsp-ui
;;   :hook (lsp-mode . lsp-ui-mode)
;;   :custom
;;   (lsp-ui-sideline-show-hover t)
;;   (lsp-ui-doc-enable nil))
;;
;; (use-package lsp-pyright
;;   :hook (python-mode . (lambda()
;;                          (require 'lsp-pyright)
;;                          (lsp))))  ; or lsp-deferred

;;(use-package lsp-treemacs
;;  :after lsp)
;; (add-hook 'python-mode-hook #'lsp)

;;;;; git-gutter
(use-package git-gutter
     :ensure t
     :init (global-git-gutter-mode))
;;;;; expand region
(use-package expand-region
  :bind
  ("C-M-=" . er/expand-region) ; I don't want to use + as it needs S-
  ("C-M--" . er/contract-region))
;;;;; pdf-tools
;; since doc-view is too slow
;; Needed to:
;; dnf install pdf-tools
;; M-x pdf-tools-install
(use-package pdf-tools
  :defer t)
;;;;; popper
(use-package popper
  :ensure t ; or :straight t
  :bind (("C-'"   . popper-toggle)
         ("M-'"   . popper-cycle)
         ("C-M-'" . popper-toggle-type))
  :init
  (setq popper-reference-buffers
        '("\\*Messages\\*"
          "\\*Warnings\\*"
          "Output\\*$"
          "\\*Async Shell Command\\*"
          "\\*Shell Command Output\\*"
          elisp-compile-mode
          Man-mode
          WoMan-mode
          help-mode
          helpful-mode
          compilation-mode
          comint-mode))
  (popper-mode +1)
  (popper-echo-mode +1)                ; For echo area hints
  (setq popper-display-function #'popper-select-popup-at-bottom)
  (setq popper-display-control 'user))

(defvar bh/star-buffer-regex
  "^\\*\\(helpful \\|Man\\|WoMan\\|info\\|Messages\\|Warnings\\|Backtrace\\|Shell Command Output\\|Compile-Log\\).*"
  "Regex to match help/info-like buffers.
Don't include *Help buffers as it messes up completion help popups.")

;; display-buffer-alist elements are (REGEX FUNCTION [PARAMS])
(when (display-graphic-p)
  (add-to-list 'display-buffer-alist
               `(,bh/star-buffer-regex (display-buffer-pop-up-frame) (reusable-frames . t))))
;;               `("^\\*compilation.*" (display-buffer-below-selected)))) ;; it already does this, in fact I can't change the behaviour!
(setq frame-auto-hide-function 'delete-frame) ;; make frames disappear on quit-window!!

;; to set it back to default:
;; (setq display-buffer-alist '((popper-display-control-p (popper-select-popup-at-bottom))))

;; experiments:
;; (advice-add 'man :after #'other-window) ; M-x man bombs!
;; (advice-remove 'man #'other-window)
;; (setq Man-notify-method "friendly")

;;;;; org-beautify
(use-package org-beautify-theme
  :ensure t)
;;;;; cc-isearch-menu (f2 in isearch)
(use-package cc-isearch-menu
  :ensure t
  :bind (:map isearch-mode-map
              ("<f2>" . cc-isearch-menu-transient)))
;;;;; polymode
(use-package polymode
  :defer t
  :ensure t
  :config
  (setq polymode-prefix-key (kbd "C-c n"))
  (define-hostmode poly-bash-hostmode :mode 'bash-mode)

  (define-innermode poly-awk-expr-bash-innermode
    :mode 'awk-mode
    :head-matcher "# awk start"
    :tail-matcher "# awk end"
    :head-mode 'host
    :tail-mode 'host)

  (defun poly-bash-awk-eval-chunk (beg end msg)
    "Calls out to `awk-send-region' with the polymode chunk region"
    (awk-send-region beg end)))

(define-polymode poly-bash-awk-mode
  :hostmode 'poly-bash-hostmode
  :innermodes '(poly-awk-expr-bash-innermode))
;;;;; transpose-frame
(use-package transpose-frame
  :bind (("C-."      . flop-frame)
         ("C->"      . flip-frame)
         ("C-<"      . rotate-frame-anticlockwise)
         ("C-,"      . transpose-frame)))
;;;;; vterm
(use-package vterm
  :ensure t
  :bind (:map vterm-mode-map
              ("C-S-v" . vterm-yank)
              ("C-S-c" . vterm-copy-mode)
              ("C-u"   . self-insert-command) ;; unix-line-discard
              ;; need to get S-<Insert> to work
              ("<f12>" . other-window)))
;;;;; dumb-jump
(use-package dumb-jump
  :defer t
  :config
  (add-hook 'xref-backend-functions #'dumb-jump-xref-activate))
;;;;; vertico
;; Enable vertico
(use-package vertico
  :defer t
  :init
  (vertico-mode)
  (vertico-buffer-mode)
  ;; Different scroll margin
  ;; (setq vertico-scroll-margin 0)

  ;; Show more candidates
  ;; (setq vertico-count 20)

  ;; Grow and shrink the Vertico minibuffer
  ;; (setq vertico-resize t)

  ;; Optionally enable cycling for `vertico-next' and `vertico-previous'.
  ;; (setq vertico-cycle t)
  )

;; Persist history over Emacs restarts. Vertico sorts by history position.
(use-package savehist
  :init
  (savehist-mode))

;; A few more useful configurations...
(use-package emacs
  :init
  ;; Add prompt indicator to `completing-read-multiple'.
  ;; We display [CRM<separator>], e.g., [CRM,] if the separator is a comma.
  (defun crm-indicator (args)
    (cons (format "[CRM%s] %s"
                  (replace-regexp-in-string
                   "\\`\\[.*?]\\*\\|\\[.*?]\\*\\'" ""
                   crm-separator)
                  (car args))
          (cdr args)))
  (advice-add #'completing-read-multiple :filter-args #'crm-indicator)

  ;; Do not allow the cursor in the minibuffer prompt
  (setq minibuffer-prompt-properties
        '(read-only t cursor-intangible t face minibuffer-prompt))
  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)
  (setq completion-cycle-threshold 3)

  ;; Emacs 28: Hide commands in M-x which do not apply to the current mode.
  ;; Corfu commands are hidden, since they are not supposed to be used via M-x.
  ;; (setq read-extended-command-predicate
  ;;       #'command-completion-default-include-p)

  ;; Enable indentation+completion using the TAB key.
  ;; `completion-at-point' is often bound to M-TAB.
  (setq tab-always-indent 'complete)

  ;; Emacs 28: Hide commands in M-x which do not work in the current mode.
  ;; Vertico commands are hidden in normal buffers.
  ;; (setq read-extended-command-predicate
  ;;       #'command-completion-default-include-p)

  ;; Enable recursive minibuffers
  (setq enable-recursive-minibuffers t))

;; Configure directory extension.
(use-package vertico-directory
  :after vertico
  :ensure nil
  ;; More convenient directory navigation commands
  :bind (:map vertico-map
              ("RET" . vertico-directory-enter)
              ("DEL" . vertico-directory-delete-char)
              ("M-DEL" . vertico-directory-delete-word))
  ;; Tidy shadowed file names
  :hook (rfn-eshadow-update-overlay . vertico-directory-tidy))
;;;;; marginalia
;; Enable rich annotations using the Marginalia package
(use-package marginalia
  ;; Bind `marginalia-cycle' locally in the minibuffer.  To make the binding
  ;; available in the *Completions* buffer, add it to the
  ;; `completion-list-mode-map'.
  :bind (:map minibuffer-local-map
         ("M-A" . marginalia-cycle))

  ;; The :init section is always executed.
  :init

  ;; Marginalia must be activated in the :init section of use-package such that
  ;; the mode gets enabled right away. Note that this forces loading the
  ;; package.
  (marginalia-mode))
;;;;; consult
;; Example configuration for Consult

(use-package consult
  ;; NB '#' prompts imply a #regex#filter eg consult-locate, consult-grep
  
  ;; Replace bindings. Lazily loaded due by `use-package'.
  :bind (;; C-c bindings in `mode-specific-map'
         ("C-c M-x" . consult-mode-command)
         ("C-c h" . consult-history)
         ("C-c k" . consult-kmacro)
         ("C-c m" . consult-man)
         ("C-c i" . consult-info)
         ([remap Info-search] . consult-info)
         ;; C-x bindings in `ctl-x-map'
         ("C-x M-:" . consult-complex-command)     ;; orig. repeat-complex-command
         ("C-x b" . consult-buffer)                ;; orig. switch-to-buffer
         ("C-x 4 b" . consult-buffer-other-window) ;; orig. switch-to-buffer-other-window
         ("C-x 5 b" . consult-buffer-other-frame)  ;; orig. switch-to-buffer-other-frame
         ("C-x t b" . consult-buffer-other-tab)    ;; orig. switch-to-buffer-other-tab
         ("C-x r b" . consult-bookmark)            ;; orig. bookmark-jump
         ("C-x p b" . consult-project-buffer)      ;; orig. project-switch-to-buffer
         ;; Custom M-# bindings for fast register access
         ("M-#" . consult-register-load)
         ("M-'" . consult-register-store)          ;; orig. abbrev-prefix-mark (unrelated)
         ("C-M-#" . consult-register)
         ;; Other custom bindings
         ("M-y" . consult-yank-pop)                ;; orig. yank-pop
         ;; M-g bindings in `goto-map'
         ("M-g e" . consult-compile-error)
         ("M-g f" . consult-flycheck)               ;; Alternative: consult-flymake
         ("M-g g" . consult-goto-line)             ;; orig. goto-line
         ("M-g M-g" . consult-goto-line)           ;; orig. goto-line
         ("M-g o" . consult-outline)               ;; Alternative: consult-org-heading
         ("M-g m" . consult-mark)
         ("M-g k" . consult-global-mark)
         ("M-g i" . consult-imenu)
         ("M-g I" . consult-imenu-multi)
         ;; M-s bindings in `search-map'
         ("M-s d" . consult-find)                  ;; Alternative: consult-fd BH - as in 'find(1)' - go up the tree with C-u prefix!!!
         ("M-s c" . consult-locate)                ;; BH - C-f5
         ("M-s g" . consult-grep)
         ("M-s G" . consult-git-grep)
         ("M-s r" . consult-ripgrep)
         ("M-s l" . consult-line)
         ("M-s L" . consult-line-multi)
         ("M-s k" . consult-keep-lines)
         ("M-s u" . consult-focus-lines)
         ;; Isearch integration
         ("M-s e" . consult-isearch-history)
         :map isearch-mode-map
         ("M-e" . consult-isearch-history)         ;; orig. isearch-edit-string
         ("M-s e" . consult-isearch-history)       ;; orig. isearch-edit-string
         ("M-s l" . consult-line)                  ;; needed by consult-line to detect isearch
         ("M-s L" . consult-line-multi)            ;; needed by consult-line to detect isearch
         ;; Minibuffer history
         :map minibuffer-local-map
         ("M-s" . consult-history)                 ;; orig. next-matching-history-element
         ("M-r" . consult-history))                ;; orig. previous-matching-history-element

  ;; Enable automatic preview at point in the *Completions* buffer. This is
  ;; relevant when you use the default completion UI.
  :hook (completion-list-mode . consult-preview-at-point-mode)

  ;; The :init configuration is always executed (Not lazy)
  :init

  ;; Optionally configure the register formatting. This improves the register
  ;; preview for `consult-register', `consult-register-load',
  ;; `consult-register-store' and the Emacs built-ins.
  (setq register-preview-delay 0.5)
  (disable setq register-preview-function #'consult-register-format) ;; does not react gracefully to invalid registers

  ;; Optionally tweak the register preview window.
  ;; This adds thin lines, sorting and hides the mode line of the window.
  (advice-add #'register-preview :override #'consult-register-window)

  ;; Use Consult to select xref locations with preview
  (setq xref-show-xrefs-function #'consult-xref
        xref-show-definitions-function #'consult-xref)

  ;; Configure other variables and modes in the :config section,
  ;; after lazily loading the package.
  :config

  ;; Optionally configure preview. The default value
  ;; is 'any, such that any key triggers the preview.
  ;; (setq consult-preview-key 'any)
  ;; (setq consult-preview-key "M-.")
  ;; (setq consult-preview-key '("S-<down>" "S-<up>"))
  ;; For some commands and buffer sources it is useful to configure the
  ;; :preview-key on a per-command basis using the `consult-customize' macro.
  (consult-customize
   consult-theme :preview-key '(:debounce 0.2 any)
   consult-ripgrep consult-git-grep consult-grep
   consult-bookmark consult-recent-file consult-xref
   consult--source-bookmark consult--source-file-register
   consult--source-recent-file consult--source-project-recent-file
   ;; :preview-key "M-."
   :preview-key '(:debounce 0.4 any)
   consult-buffer :prompt "Switch to [*|f|b|m|p|r <SPC>]:")

  ;; Optionally configure the narrowing key.
  ;; Both < and C-+ work reasonably well.
  (setq consult-narrow-key "<") ;; "C-+"

  ;; Optionally make narrowing help available in the minibuffer.
  ;; You may want to use `embark-prefix-help-command' or which-key instead.
  ;; (define-key consult-narrow-map (vconcat consult-narrow-key "?") #'consult-narrow-help)

  ;; By default `consult-project-function' uses `project-root' from project.el.
  ;; Optionally configure a different project root function.
  ;;;; 1. project.el (the default)
  ;; (setq consult-project-function #'consult--default-project--function)
  ;;;; 2. vc.el (vc-root-dir)
  ;; (setq consult-project-function (lambda (_) (vc-root-dir)))
  ;;;; 3. locate-dominating-file
  ;; (setq consult-project-function (lambda (_) (locate-dominating-file "." ".git")))
  ;;;; 4. projectile.el (projectile-project-root)
  ;; (autoload 'projectile-project-root "projectile")
  ;; (setq consult-project-function (lambda (_) (projectile-project-root)))
  ;;;; 5. No project support
  ;; (setq consult-project-function nil)
  )

;; from https://www.reddit.com/r/emacs/comments/1db1hzd/consult_error_on_windowconfigurationtoregister/
(defun bh/clean-register-alist (&optional noerror filter)
  "Clean `register-alist` by removing entries where any part of the value
is a marker in a buffer which no longer exists.
This function is intended to be used as a `before` advice for
`consult-register--alist`. The arguments NOERROR and FILTER are included
to match the advised function's signature but are not used."
  (setq register-alist
        (cl-remove-if
         (lambda (entry)
           (let ((value (cdr entry)))
             (or
              ;; Check if the value itself is an invalid marker.
              (and (markerp value)
                   (not (marker-buffer value)))
              ;; Check if the value contains invalid markers.
              (and (consp value)
                   (seq-some
                    (lambda (item)
                      (and (markerp item)
                           (not (marker-buffer item))))
                    value)))))
         register-alist)))
(advice-add 'consult-register--alist :before #'bh/clean-register-alist)

;;;;; orderless
(use-package orderless
  :ensure t
  :init
  ;; Configure a custom style dispatcher (see the Consult wiki)
  ;; (setq orderless-style-dispatchers '(+orderless-consult-dispatch orderless-affix-dispatch)
  ;;       orderless-component-separator #'orderless-escapable-split-on-space)
  (setq completion-styles '(orderless basic)
        completion-category-defaults nil
        completion-category-overrides '((file (styles partial-completion)))))
;;;;; corfu
(use-package corfu
  :ensure t
  ;; Optional customizations
  :custom
  (corfu-cycle t)                ;; Enable cycling for `corfu-next/previous'
  (corfu-auto nil)                  ;; Enable auto completion
  (corfu-separator ?\s)          ;; Orderless field separator
  ;; (corfu-quit-at-boundary nil)   ;; Never quit at completion boundary
  (corfu-quit-no-match 'separator)
  ;; (corfu-preview-current nil)    ;; Disable current candidate preview
  ;; (corfu-preselect 'prompt)      ;; Preselect the prompt
  ;; (corfu-on-exact-match nil)     ;; Configure handling of exact matches
  ;; (corfu-scroll-margin 5)        ;; Use scroll margin
  :bind
  (:map corfu-map
        ("SPC" . corfu-insert-separator)
        ("C-n" . corfu-next)
        ("C-p" . corfu-previous)
        ("M-q" . corfu-quick-complete)
        ("C-q" . corfu-quick-insert))
  ;;    ("RET" . nil)))

  ;; Recommended: Enable Corfu globally.  This is recommended since Dabbrev can
  ;; be used globally (M-/).  See also the customization variable
  ;; `global-corfu-modes' to exclude certain modes.
  :init
  (global-corfu-mode)
  (corfu-popupinfo-mode nil)
  (corfu-history-mode 1))
(add-to-list 'savehist-additional-variables 'corfu-history)

;; Part of corfu
(use-package corfu-popupinfo
  :after corfu
  :ensure nil ;; otherwise it does a package-refresh-contents!!
  :hook (corfu-mode . corfu-popupinfo-mode)
  :custom
  (corfu-popupinfo-delay '(0.25 . 0.1))
  (corfu-popupinfo-hide nil)
  :config
  (corfu-popupinfo-mode))

;;;;; consult-dir
(use-package consult-dir
  :ensure t
  :bind (("C-x C-d" . consult-dir)
         :map vertico-map
         ("C-x C-d" . consult-dir)
         ("C-x C-j" . consult-dir-jump-file)))

;;;;; cape

;; Install and configure Cape for additional completion sources
(use-package cape
  :ensure t
  :init
  ;; Add `cape-dabbrev` to `completion-at-point-functions`
  (add-hook 'completion-at-point-functions #'cape-dabbrev)
  ;; Optionally, add other useful Cape functions
  (add-hook 'completion-at-point-functions #'cape-file)
  (add-hook 'completion-at-point-functions #'cape-keyword)
  (add-hook 'completion-at-point-functions #'cape-elisp-block)
  ;;(add-hook 'completion-at-point-functions #'cape-history)
  ;;(add-hook 'completion-at-point-functions #'cape-keyword)
  ;;(add-hook 'completion-at-point-functions #'cape-tex)
  ;;(add-hook 'completion-at-point-functions #'cape-sgml)
  ;;(add-hook 'completion-at-point-functions #'cape-rfc1345)
  ;;(add-hook 'completion-at-point-functions #'cape-abbrev)
  ;;(add-hook 'completion-at-point-functions #'cape-dict)
  ;;(add-hook 'completion-at-point-functions #'cape-elisp-symbol)
  ;;(add-hook 'completion-at-point-functions #'cape-line)
  ;; More Cape functions can be added similarly if needed
  )

;; Optionally, configure completion styles for better results
(setq completion-styles '(orderless basic partial-completion flex))

;;;;; bh/grep-buffers
;; https://emacs.stackexchange.com/questions/737/how-do-i-find-text-across-many-open-buffers @20240222
(defun bh/grep-buffers (regexp)
  (interactive "sRegex: ")
  (multi-occur-in-matching-buffers "." regexp nil))
(global-set-key (kbd "C-c g") 'bh/grep-buffers)

;;;;; dired-sidebar and ibuffer-sidebar
;; can't get this to follow the buffer! - use speedbar?
;; (use-package dired-sidebar
;;   :bind (("C-x C-n" . dired-sidebar-toggle-sidebar))
;;   :ensure t
;;   :commands (dired-sidebar-toggle-sidebar)
;;   :init
;;   (add-hook 'dired-sidebar-mode-hook
;;             (lambda ()
;;               (unless (file-remote-p default-directory)
;;                 (auto-revert-mode))))
;;   :config
;;   (push 'toggle-window-split dired-sidebar-toggle-hidden-commands)
;;   (push 'rotate-windows dired-sidebar-toggle-hidden-commands)
;;   (setq dired-sidebar-should-follow-file t)
;;   (setq dired-sidebar-subtree-line-prefix "__")
;;   (setq dired-sidebar-theme 'vscode)
;;   (setq dired-sidebar-use-term-integration t)
;;   (setq dired-sidebar-no-delete-other-windows t)
;;   (setq dired-sidebar-use-custom-font nil))

(use-package ibuffer-sidebar
  :load-path "~/.config/emacs/ibuffer-sidebar"
  :ensure nil
  :commands (ibuffer-sidebar-toggle-sidebar)
  :config
  (setq ibuffer-sidebar-use-custom-font nil)
  (setq ibuffer-sidebar-face `(:family "Helvetica" :height 140)))
;; (defun +sidebar-toggle ()
;;   "Toggle both `dired-sidebar' and `ibuffer-sidebar'."
;;   (interactive)
;;   (dired-sidebar-toggle-sidebar)
;;   (ibuffer-sidebar-toggle-sidebar))
;;;;; yasnippet
;; just too slow at startup; never use it
;; (use-package yasnippet
;;   :config
;;   (yas-global-mode 1))
;; (use-package yasnippet-snippets)

;;;;; ediff

(setq ediff-split-window-function #'split-window-horizontally
      ediff-window-setup-function #'ediff-setup-windows-plain ;; prefer pop-up window in GUI
      ediff-keep-variants nil)

(defun bh/ediff-prepare-buffer ()
  "stop outline-mode"
  (interactive)
  (when outline-minor-mode
    (outline-show-all)))
(add-hook 'ediff-prepare-buffer-hook #'bh/ediff-prepare-buffer)

;; from http://yummymelon.com/devnull/using-ediff-in-2023.html
(defun cc/stash-window-configuration-for-ediff ()
  "Store window configuration to register 🧊.
Use of emoji is to avoid potential use of keyboard character to reference
the register."
  (window-configuration-to-register ?🧊))

(defun cc/restore-window-configuration-for-ediff ()
  "Restore window configuration from register 🧊.
Use of emoji is to avoid potential use of keyboard character to reference
the register."
  (jump-to-register ?🧊))

(add-hook 'ediff-before-setup-hook #'cc/stash-window-configuration-for-ediff)
;; !!!: CC Note: Why this is not `ediff-quit-hook' I do not know. But this works
;; for cleaning up ancillary buffers on quitting an Ediff session.
(add-hook 'ediff-after-quit-hook-internal #'cc/restore-window-configuration-for-ediff)

;;;;; treesit-auto
;; https://github.com/renzmann/treesit-auto
;; at 29.3 pgtk treesitter is "stack smashing" in python_imenu_treesit_create_index:
;(use-package treesit-auto
;  :custom
;  (treesit-auto-install 'prompt)
;  :config
;  (treesit-auto-add-to-auto-mode-alist 'all)
;  (global-treesit-auto-mode))

;;;;; markdown mode
;; (eg for README.md - includes gfm-mode)
(use-package markdown-mode
  :ensure t
  :mode ("README\\.md\\'" . gfm-mode)
  :init (setq markdown-command "multimarkdown"))

;;;;; Load mu4e
(use-package mu4e
  :load-path "/usr/share/emacs/site-lisp/mu4e" ;; Adjust the path if necessary
  :ensure nil
  :config
  ;; NB maildir is set in 'mu' which defaults to ~/Maildir - so these lines do nothing:
  ;;(setq mu4e-root-maildir "/net/aloo/export/fedora-backup/backup-copy/achar/2024/09/02-08_29/home/bhepple/Maildir/")
  ;;(setq mu4e-maildir "$HOME/Maildir")
  
  (setq mu4e-drafts-folder "/draft")
  (setq mu4e-sent-folder   "/sent")
  (setq mu4e-trash-folder  "/trash")
  (setq mu4e-refile-folder nil)

  ;; This enables mail syncing with isync (mbsync) if you prefer that over offlineimap
  ;; (setq mu4e-get-mail-command "mbsync -a")
  (setq mu4e-get-mail-command "offlineimap")
  (setq mu4e-update-interval 300)
  (setq mu4e-view-show-images t)
  (setq mu4e-view-show-addresses t)
  (setq mu4e-compose-signature-auto-include nil)

  ;; SMTP settings
  (setq message-send-mail-function 'smtpmail-send-it
        smtpmail-starttls-credentials '(("smtp.gmail.com" 587 nil nil))
        smtpmail-auth-credentials '(("smtp.gmail.com" 587 "bob.hepple@gmail.com" nil))
        smtpmail-default-smtp-server "smtp.gmail.com"
        smtpmail-smtp-server "smtp.gmail.com"
        smtpmail-smtp-service 587)

  ;; Set up your user mail address and name
  (setq user-mail-address "bob.hepple@gmail.com")
  (setq user-full-name "Bob Hepple")

  ;; Customize how mu4e shows threads
  (setq mu4e-headers-skip-duplicates t)
  (setq mu4e-headers-show-threads t)
  (setq mu4e-headers-auto-update t)
  
  ;; Integrate with org-mode
  ;; (require 'org-mu4e)

  ;; use vertico/orderless for completion:
  (setq mu4e-read-option-use-builtin nil
        mu4e-completing-read-function 'completing-read)

  ;; Optional: Org-mode email capture template
  (setq org-mu4e-link-query-in-headers-mode nil)
  (setq org-capture-templates
        '(("m" "Email" entry (file+headline "~/org/inbox.org" "Emails")
           "* TODO [#A] Respond to %:fromname on %:subject\nSCHEDULED: %t\n%a\n\n%i"
           :immediate-finish t)))
  (setq mu4e-headers-fields
        '((:human-date . 12)    ;; Date
          (:flags . 6)          ;; Flags
          (:size . 8)           ;; Size
          (:maildir . 50)       ;; Labels (Maildir path)
          (:from-or-to . 25)    ;; Sender or recipient
          (:subject)))          ;; Subject

  ;; View HTML message in the browser
  (setq mu4e-html2text-command 'mu4e-shr2text)
  (setq mu4e-view-prefer-html t)

  ;; Key bindings for mu4e
  (global-set-key (kbd "C-c M") 'mu4e))

;;;;; casual-re-builder
(use-package casual-re-builder
  :ensure t
  :bind (:map reb-mode-map
              ("C-o" . casual-re-builder-tmenu)))

;; ;;;;; embark
;; (use-package embark
;;   :ensure t

;;   :bind
;;   (("C-." . embark-act)         ;; pick some comfortable binding
;;    ("C-;" . embark-dwim)        ;; good alternative: M-.
;;    ("C-h B" . embark-bindings)) ;; alternative for `describe-bindings'

;;   :init

;;   ;; Optionally replace the key help with a completing-read interface
;;   (setq prefix-help-command #'embark-prefix-help-command)

;;   ;; Show the Embark target at point via Eldoc. You may adjust the
;;   ;; Eldoc strategy, if you want to see the documentation from
;;   ;; multiple providers. Beware that using this can be a little
;;   ;; jarring since the message shown in the minibuffer can be more
;;   ;; than one line, causing the modeline to move up and down:

;;   ;; (add-hook 'eldoc-documentation-functions #'embark-eldoc-first-target)
;;   ;; (setq eldoc-documentation-strategy #'eldoc-documentation-compose-eagerly)

;;   :config

;;   ;; Hide the mode line of the Embark live/completions buffers
;;   (add-to-list 'display-buffer-alist
;;                '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
;;                  nil
;;                  (window-parameters (mode-line-format . none)))))

;; ;; Consult users will also want the embark-consult package.
;; (use-package embark-consult
;;   :ensure t ; only need to install it, embark loads it after consult if found
;;   :hook
;;   (embark-collect-mode . consult-preview-at-point-mode))

;;; languages
;;;; c and c++ mode stuff

;; this is common for c and c++. Maybe java?

;; From the info page and stroustrup style and edited:
(defconst bh/c-style
  '((c-tab-always-indent        . 'complete)
    (c-comment-only-line-offset . 0)
    (c-hanging-braces-alist     . ((substatement-open before after)
                                   (brace-list-open)))
    (c-hanging-colons-alist     . ((member-init-intro before)
                                   (inher-intro)
                                   (case-label after)
                                   (label after)
                                   (access-label after)))
;;   (c-cleanup-list             . (scope-operator
;;                                  empty-defun-braces
;;                                  defun-close-semi))
    (c-offsets-alist            . ((statement-block-intro . +)
                                   (substatement-open . 0)
                                   (label . 0)
                                   (statement-cont . +)
                                   (case-label . 8)))
;;  (c-offsets-alist            . ((arglist-close . c-lineup-arglist)
;;                                 (substatement-open . 0)
;;                                 (case-label        . 4)
;;                                 (block-open        . 0)
;;                                 (knr-argdecl-intro . -)))
    (c-echo-syntactic-information-p . t)
    (indent-tabs-mode . nil))
  "My C Programming Style" )

(defun bh/c-mode-fiddle ()
  (c-add-style "PERSONAL" bh/c-style t)
  (eval-when-compile (setq c-auto-newline nil)) ; auto-add a newline, yuck!
  (c-toggle-auto-newline -1) ; turn off auto-newline
  (setq indent-tabs-mode nil) ; use spaces for indentation
  (setq tab-width 4)
  ;; (hs-minor-mode) ; hide/show mode - better than outline-mode for C?
  ;; (hs-show-all)
  ;; (define-key c-mode-map (kbd "TAB") 'indent-for-tab-command)
  ;; (define-key c-mode-base-map (kbd "TAB") 'indent-for-tab-command)
;;  (define-key c-mode-map (kbd "C-<return>") 'c-mark-function)
;;  (define-key c-mode-base-map (kbd "C-m") 'c-context-line-break) ; make RET act like ^J!! and indent
;;  (c-set-offset 'arglist-cont-nonempty 'c-lineup-cont) ; for RSA
  (font-lock-mode)

;(c-set-style "stroustrup")
;(setq c-tab-always-indent nil) ; Only indent if cursor is at left side
;; for auto-newline _and_ hungry-delete:
;; (c-toggle-auto-hungry-state 1)

;;  (setq c-indent-level indent)
;;  (setq c-basic-offset indent)
;;  (setq c-continued-statement-offset indent)
;;  (setq c-brace-imaginary-offset 0)
;;  (setq c-continued-brace-offset (* -1 indent))
;;  (setq c-brace-offset 0)
;;  (setq c-argdecl-indent indent)
;;  (setq c-label-offset (* -1 indent))
  )

;;(setq auto-mode-alist (cons '("\\.C$" . c++-mode) auto-mode-alist))
;;(setq auto-mode-alist (cons '("\\.cc$" . c++-mode) auto-mode-alist))
(add-hook 'c-mode-common-hook 'bh/c-mode-fiddle)

;; This is the example from the info page:
(defun c-lineup-streamop (langelem)
;; lineup stream operators
  (save-excursion
    (let* ((relpos (cdr langelem))
           (curcol (progn (goto-char relpos)
                          (current-column))))
      (re-search-forward "<<\\|>>" (c-point 'eol) 'move)
      (goto-char (match-beginning 0))
      (- (current-column) curcol))))

;(defun c-lineup-cont (langelem)
;;  ;; lineup continuation lines simply by adding the offset - but from
;;  ;; the start of the prior line rather than syntactically - for RSA!!
;;  (save-excursion
;;   (let* ((relpos (cdr langelem))
;;          (curcol (progn (goto-char relpos)
;;                         (current-column))))
;;     (goto-char (c-point 'bol))
;;     (re-search-forward "[^    ]" (c-point 'eol) 'move)
;;     (goto-char (match-beginning 0))
;;     (+ c-basic-offset (- (current-column) curcol)))))

;;
;; Set Stroustrup C/C++/Java coding style
;;
(add-hook 'c-mode-hook
          #'(lambda ()
             (c-set-style "Stroustrup")
             (setq c-electic-flag nil)))

(add-hook 'c++-mode-hook
          #'(lambda ()
             (c-set-style "Stroustrup")
             (setq c-electic-flag nil)))

(add-hook 'awk-mode-hook
          #'(lambda ()
             (c-set-style "Stroustrup")
             (setq c-electic-flag nil)))

(add-hook 'java-mode-hook
          #'(lambda ()
             (c-set-style "Stroustrup")))

;;;; shell-script-mode:

(defun bh/sh-indent-rule-for-close-brace ()
  (when (eq ?= (char-before))
    (skip-chars-backward "[:alnum:]_=")
    (current-column)))
;(add-hook 'sh-mode-hook
;;          (lambda ()
;;            (add-hook 'smie-indent-functions
;;                      #'bh/sh-indent-rule-for-close-brace
;;                      nil 'local)))

(defun bh/sh-indent-rules ()
  (setq sh-use-smie nil)
  (setq sh-styles-alist
        '(("bobs"
           (sh-basic-offset . 4)
           (sh-first-lines-indent . 0)
           (sh-indent-after-case . +)
           (sh-indent-after-do . +)
           (sh-indent-after-done . 0)
           (sh-indent-after-else . +)
           (sh-indent-after-if . +)
           (sh-indent-after-loop-construct . +)
           (sh-indent-after-open . +)
           (sh-indent-comment . t)
           (sh-indent-for-case-alt . ++)
           (sh-indent-for-case-label . +)
           (sh-indent-for-continuation . +)
           (sh-indent-for-do . 0)
           (sh-indent-for-done . 0)
           (sh-indent-for-else . 0)
           (sh-indent-for-fi . 0)
           (sh-indent-for-then . 0))))
  (sh-load-style "bobs"))
(add-hook 'sh-mode-hook #'bh/sh-indent-rules)

(defun bh/sh-smie-rules (orig-fun kind token)
  (pcase (cons kind token)
      (`(:before . "|") nil)
    (_ (funcall orig-fun kind token))))
(advice-add 'sh-smie-sh-rules :around #'bh/sh-smie-rules)

(defun bh/sh-mode-customisations ()
  (hs-minor-mode)
  (hs-show-all))
(add-hook 'sh-mode-hook #'bh/sh-mode-customisations)

;;;; nroff etc
;; Set appropriate flags for nroff documents - errors in latest versions
(defun bh/nroff-mode-hook ()
  (setq ispell-filter-hook-args '("-w"))
  (setq ispell-filter-hook "deroff")
  (setq ispell-words-have-boundaries nil)
  (define-key nroff-mode-map (kbd "C-b") #'(lambda ()
                                            (interactive)
                                            (insert "\\fB\\fP")
                                            (backward-char 3)))
  (define-key nroff-mode-map (kbd "C-i") #'(lambda ()
                                            (interactive)
                                            (insert "\\fI\\fP")
                                            (backward-char 3)))
  (define-key nroff-mode-map [f8] 'man-preview))
(add-hook 'nroff-mode-hook 'bh/nroff-mode-hook)
(setq auto-mode-alist (cons '("\\.1$" . nroff-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.1m$" . nroff-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.me$" . nroff-mode) auto-mode-alist))

(autoload 'man-preview "man-preview" nil t)

;;;; python
(add-hook 'python-mode-hook #'bh/python-mode-hook)
(defun bh/python-mode-hook ()
  "the imenu and outline functions in python-mode just don't work for me"
  (interactive)
;;  (define-key flymake-mode-map (kbd "C-<f7>") #'flymake-goto-next-error)
;;  (define-key flymake-mode-map (kbd "S-C-<f7>") #'flymake-goto-prev-error)
  (setq-local imenu-generic-expression '((nil "^[ \t]*\\(def\\|class\\) \\(.*\\)$" 2)))
  (setq-local imenu-create-index-function 'imenu-default-create-index-function)
  (setq-local outline-regexp "\\(\\`\\|[ \t]*\\(def\\|class\\) \\|if \\)")
  (setq-local outline-minor-mode-cycle t))

;;;; lisp
; might be fun to play with instead of the one I had in 'local variables'
;; from https://www.emacs.dyerdwelling.family/emacs/20230414111409-emacs--indexing-emacs-init/
(add-hook 'emacs-lisp-mode-hook
 (lambda ()
   (setq-local imenu-sort-function 'imenu--sort-by-name)
   (setq-local imenu-case-fold-search t) ; generic-expr regexp, not the order that items appear
   (setq-local imenu-auto-rescan nil)
   (setq-local imenu-generic-expression
               '((nil "^.*([[:space:]]*defun[[:space:]]+\\([[:word:]-/]+\\)" 1)))
   (imenu-add-menubar-index)))
;;; functions
;;;; iflipb
(defun bh/iflipb-ignore-buffers (buf)
  "what buffers to ignore in iflipb"
  (cond
   ((equal "*elfeed-search*" buf) nil)
   ((string-match "^[*]" buf) t)
   (:otherwise nil)))
(use-package iflipb
  :config
  (setq-default iflipb-ignore-buffers 'bh/iflipb-ignore-buffers)
  :bind
  ("<C-tab>" . iflipb-next-buffer)
  ("<C-S-iso-lefttab>" . iflipb-previous-buffer))

;;;; cycle whitespace-mode

;; these defaults for whitespace settings were snarfed directly from doom:
(defun doom-highlight-non-default-indentation-h ()
  "Highlight whitespace at odds with `indent-tabs-mode'.
That is, highlight tabs if `indent-tabs-mode' is `nil', and highlight spaces at
the beginnings of lines if `indent-tabs-mode' is `t'. The purpose is to make
incorrect indentation in the current buffer obvious to you.

Does nothing if `whitespace-mode' or `global-whitespace-mode' is already active
or if the current buffer is read-only or not file-visiting."
  (interactive)
  (unless (or (eq major-mode 'fundamental-mode)
              (bound-and-true-p global-whitespace-mode)
              (null buffer-file-name))
    (require 'whitespace)
    (set (make-local-variable 'whitespace-style)
         (cl-union (if indent-tabs-mode
                       '(indentation)
                     '(tabs tab-mark))
                   (when whitespace-mode
                     (remq 'face whitespace-active-style))))
    (cl-pushnew 'face whitespace-style) ; must be first
    (whitespace-mode +1)))

;; cycle whitespace-mode between whitespace.el defaults, doom's defaults and off:
;; off=>default=>doom=>off...
(defun bh/toggle-whitespace () (interactive)
       (cond ((string= bh/whitespace-mode "doom")
              (setq bh/whitespace-mode "off")
              (princ "whitespace-mode is off")
              (whitespace-mode -1))
             ((string= bh/whitespace-mode "default")
              (setq bh/whitespace-mode "doom")
              (princ "whitespace-mode is doom's 'highlight non-default indentation'")
              (whitespace-mode -1) ; so that doom-highlight-non-default-indentation-h does something
              (doom-highlight-non-default-indentation-h))
             (t ; (string= bh/whitespace-mode "off")
              (setq whitespace-style (default-value 'whitespace-style)
                    whitespace-display-mappings (default-value 'whitespace-display-mappings)
                    bh/whitespace-mode "default")
              (princ "whitespace-mode is default")
              (whitespace-mode +1))))

(setq tabify-regexp " [ \t]+")
;;;; scroll on mouse wheel

(defun bh/up-slightly ()   (interactive) (scroll-up 5))
(defun bh/down-slightly () (interactive) (scroll-down 5))
(defun bh/up-one ()        (interactive) (scroll-up 1))
(defun bh/down-one ()      (interactive) (scroll-down 1))
(defun bh/up-a-lot ()      (interactive) (scroll-up))
(defun bh/down-a-lot ()    (interactive) (scroll-down))

(global-set-key  (kbd "S-<mouse4>") 'bh/down-one)
(global-set-key  (kbd "S-<mouse5>") 'bh/up-one)
(global-set-key  (kbd "<mouse4>")   'bh/down-slightly)
(global-set-key  (kbd "<mouse5>")   'bh/up-slightly)
(global-set-key  (kbd "C-<mouse4>") 'bh/down-a-lot)
(global-set-key  (kbd "C-<mouse5>") 'bh/up-a-lot)

;; emacs-pgtk
;(global-set-key  (kbd "<wheel-down>")   'scroll-bar-toolkit-scroll)
;(global-set-key  (kbd "<wheel-up>")   'bh/up-slightly)

;;;; gjots mode
;; remaining problems:
;; you must show-all before saving
;; it prompts for coding. default raw-text is OK
(setq format-alist
      (cons '(gjots "gjots" nil "gjots2org" "org2gjots" t nil) format-alist))
(define-derived-mode gjots-mode org-mode "gjots"
  "Major mode for editing gjots files."
  (format-decode-buffer 'gjots)
  (outline-hide-sublevels '1))
;(autoload 'gjots-mode "gjots-mode" "Major mode to edit gjots files." t)
;(setq auto-mode-alist
;;      (cons '("\\.gjots$" . gjots-mode) auto-mode-alist))

;;;; syntax checking on file save:
(defun bh/compile (command &optional comint)
  "Wrap `compile' so that variable `compile-command' is preserved.

If optional second arg COMINT is t the buffer will be in Comint mode with
`compilation-shell-minor-mode' (as in `compile')."
  (setq bh/before-compile-window-configuration (current-window-configuration))
  (setq bh/before-compile-point (point))
  (setq temp-compile-command compile-command)
  (setq compile-command command)
  (compile command comint)
  (setq compile-command temp-compile-command))
  
(defun bh/check-syntax ()
  "Check syntax for various languages."
  (interactive)
  (when (and buffer-file-name (not (bound-and-true-p no-syntax-check)))
    (when (eq major-mode 'emacs-lisp-mode)
      (ignore-errors (kill-buffer byte-compile-log-buffer))
      (let ((byte-compile-warnings '(not free-vars obsolete unresolved)))
        (unless (byte-compile-file buffer-file-name)
            (display-buffer byte-compile-log-buffer))))
    (when (eq major-mode 'sh-mode)
      (bh/compile (format "bash -n %s && shellcheck -f gcc %s" buffer-file-name buffer-file-name) nil))
    (when (eq major-mode 'ruby-mode)
      (bh/compile (format "ruby -c %s" buffer-file-name) nil))
    (when (eq major-mode 'python-mode)
      (whitespace-cleanup)
      (bh/compile (format "python -B -m py_compile %s" buffer-file-name) nil))
    (when (eq major-mode 'awk-mode)
      (bh/compile (format "AWKPATH=$PATH gawk --lint --source 'BEGIN { exit(0) } END { exit(0) }' --file %s" buffer-file-name) nil))))

;; This stops compile window from opening if there are no errors.
;; from https://stackoverflow.com/questions/8309769/how-can-i-prevent-emacs-from-opening-new-window-for-compilation-output
(defun bh/compilation-exit-autoclose (STATUS code msg)
  "Close the compilation window if there was no error at all."
  ;; If M-x compile exists with a 0
  (when (and (eq STATUS 'exit) (zerop code))
    ;; then bury the *compilation* buffer, so that C-x b doesn't go there
    (bury-buffer)
    ;; and delete-char the *compilation* window
    (delete-window (get-buffer-window (get-buffer "*compilation*")))
    ;;(set-window-configuration bh/before-compile-window-configuration) ;; prevents file being saved!!!
    ;;(goto-char bh/before-compile-point)
    (message "no errors"))
  ;; Always return the anticipated result of compilation-exit-message-function
  (cons msg code))
(setq compilation-exit-message-function 'bh/compilation-exit-autoclose)

(add-hook 'next-error-hook #'(lambda()
                               (if (outline-minor-mode)
                                   (outline-show-all))))
(add-hook 'after-save-hook #'bh/check-syntax)

;;;; recently-used.xbel

(defun bh/save-to-recently-used ()
  "Calls recentf-gtk to update ~/.local/share/recently-used.xbel"
  (when (online?) ;; otherwise network files cause a hang
    (let ((save-to-recently-used-exclusions '("COMMIT_EDITMSG" "^/tmp/" "^/sudo:root" "/ido.last")))
      (unless (cl-some (lambda (exclusion)
                         (string-match-p exclusion buffer-file-name))
                       save-to-recently-used-exclusions)
        (call-process "sh" nil 0 nil "-c" (concat
                                           "recentf-gtk --mime-type `file -ib "
                                           buffer-file-name
                                           "|sed 's/;.*//' | tr -d '\n'` --app-name emacs add "
                                           buffer-file-name))))))
      ;;(call-process "xeyes" nil 0 nil))))
(add-hook 'after-save-hook #'bh/save-to-recently-used)

;;;; unfill paragraph
;; from https://www.emacswiki.org/emacs/UnfillParagraph:
    ;;; Stefan Monnier <foo at acm.org>. It is the opposite of fill-paragraph
(defun unfill-paragraph (&optional region)
  "Takes a multi-line paragraph and makes it into a single line of text."
  (interactive (progn (barf-if-buffer-read-only) '(t)))
  (let ((fill-column (point-max))
        ;; This would override `fill-column' if it's an integer.
        (emacs-lisp-docstring-fill-column t))
    (fill-paragraph nil region)))
;; Handy key definition
(define-key global-map "\M-Q" 'unfill-paragraph)
;;;; terminal
(defun bh/term-and-yank-pwd (&optional arg)
  "Open a vterm with pwd in clipboard.

With C-u, pops up a new frame with vterm.
With C-u C-u pops up a new myterm process.
Otherwise, opens vterm in another (emacs) window."
  (interactive "P")
  (bh/pwd-to-clipboard)
  (if (equal arg '(16))
      (start-process "term" nil "myterm")
    (progn
      (unless (get-buffer "*vterm*")
        (vterm-other-window))
      (if (equal arg '(4))
          (switch-to-buffer-other-frame (get-buffer "*vterm*"))
        (pop-to-buffer "*vterm*")))))
;;;; stupid windows box:
;;(when (getenv "OS")
;;  (when (string-match "Windows.*" (getenv "OS"))
;;    (eval-when-compile (setq locate-command "/home/mobaxterm/bin/mylocate"))
;;    (setq select-enable-clipboard "true")
;;    (set-background-color "black")
;;    (set-foreground-color "white")))

;;;; Misc functions
(defun bh/auto-deploy ()
  "install it"
  (interactive)
  (shell-command (concat "cp " buffer-file-name " " (getenv "HOME") "/media/photos/cgi-bin/")))

(defun bh/just-tab ()
  "Just put a fecking tab in, for chrisake!"
  (interactive)
  (insert "\t"))

(defun bh/kill-next-window ()
  "As it says - mainly for when we're full screen"
  (interactive)
  (other-window 1)
  (delete-window))

(defun bh/sudo-find-file (file-name) "Like find file, but opens the file as root."
       (interactive "FSudo Find File: ")
       (let ((tramp-file-name (concat "/sudo::" (expand-file-name file-name))))
         (find-file tramp-file-name)))

(defun bh/paren-match ()
  "Jumps to the paren matching the one under point, and does nothing if there
 isn't one."
  (interactive)
  (cond ((looking-at "[\(\[{]")
         (forward-sexp 1)
         (backward-char))
        ((looking-at "[])}]")
         (forward-char)
         (backward-sexp 1))
        (t (message "Could not find matching paren."))))

(defun bh/toggle-case-fold ()
  "Toggle the variable case-fold-search."
  (interactive)
  (if (equal case-fold-search nil)
      (progn
        (setq case-fold-search t)
        (message "case-fold-search set to TRUE"))
    (progn
      (setq case-fold-search nil)
      (message "case-fold-search set to nil"))))

(defun bh/toggle-tab-width ()
  "Toggle tab-width between 4 and 8."
  (interactive)
  (if (equal tab-width 4)
      (progn
        (setq tab-width 8)
        (message "tab-width set to 8"))
    (progn
      (setq tab-width 4)
      (message "tab-width set to 4")))
  (recenter-top-bottom))

(defun bh/yank-whole-line (&optional arg)
  "Copy the current line to the kill ring.
With a prefix argument (C-u), append the line to the last kill in the ring."
  (interactive "P")
  (let ((beg (line-beginning-position))
        (end (line-beginning-position 2)))
    (if arg
        (kill-append (buffer-substring beg end) nil)
      (kill-ring-save beg end))))

(defun bh/dup-line ()
  "Duplicate the current line line and move point to the same
 column on the new line. May need to customize
 `duplicate-line-final-position'"
  (interactive)
  (let ((col (current-column)))
    (duplicate-line)
    (move-to-column col)))

(defun bh/kill-whole-line (&optional arg)
  "Delete the current line. With C-u, append the line to the last kill in the ring."
       (interactive "P")
       (beginning-of-line nil)
       (if arg
           (append-next-kill))
       (kill-line 1))

(defun bh/time-stamp () "Print a string like 20200830-122439"
       (interactive)
       (insert (format-time-string "%Y%m%d-%H%M%S")))

(defun bh/date-stamp () "Print s string like Sun Aug 30, 2020 12:25"
       (interactive)
       (insert (format-time-string "%a %b %e, %Y %H:%M ")))

(defun bh/insert-x-selection () ""
       (interactive)
       (insert (gui-get-selection 'PRIMARY 'STRING)))

(defun bh/insert-x-clipboard () ""
       (interactive)
       (insert (gui-get-selection 'CLIPBOARD 'STRING)))

(defun bh/pwd-to-clipboard () ""
       (interactive)
       (let ((tmp (pwd)))
         (if buffer-file-name
             (setq tmp (file-name-directory buffer-file-name))
           (if (string-match "^Directory " tmp)
             (setq tmp (replace-match "" t t tmp))))
         (kill-new tmp)))
         ;; was (gui-select-text tmp)))

;; from https://emacs.stackexchange.com/questions/46664/switch-between-horizontal-and-vertical-splitting
(defun bh/toggle-window-split ()
  (interactive)
  (if (= (count-windows) 2)
      (let* ((this-win-buffer (window-buffer))
             (next-win-buffer (window-buffer (next-window)))
             (this-win-edges (window-edges (selected-window)))
             (next-win-edges (window-edges (next-window)))
             (this-win-2nd (not (and (<= (car this-win-edges)
                                         (car next-win-edges))
                                     (<= (cadr this-win-edges)
                                         (cadr next-win-edges)))))
             (splitter
              (if (= (car this-win-edges)
                     (car (window-edges (next-window))))
                  'split-window-horizontally
                'split-window-vertically)))
        (delete-other-windows)
        (let ((first-win (selected-window)))
          (funcall splitter)
          (if this-win-2nd (other-window 1))
          (set-window-buffer (selected-window) this-win-buffer)
          (set-window-buffer (next-window) next-win-buffer)
          (select-window first-win)
          (if this-win-2nd (other-window 1))))))

(defun bh/buffer-menu (&optional arg)
  "runs buffer-menu but with the sense of C-u inverted (ie files only
unless C-u is given)"
  (interactive "P")
  (setq arg (not arg))
  (buffer-menu arg))

;; from http://mbork.pl/2014-04-04_Fast_buffer_switching_and_friends
(defun bh/switch-bury-or-kill-buffer (&optional aggr)
  "With no argument, switch (but unlike C-x b, without the need
to confirm).  With C-u, bury current buffer.  With double C-u,
kill it (unless it's modified)."
  (interactive "P")
  (cond
   ((eq aggr nil)
    (progn
      (cl-dolist (buf '("*Buffer List*" "*Ibuffer*" "*Bookmark List* " "*vc-change-log*" "*Locate*" "*grep*" "*compilation*" "*log-edit-files*"))
        (when (get-buffer buf)
          (bury-buffer buf)))
      (switch-to-buffer (other-buffer))))
   ((equal aggr '(4)) (bury-buffer))
   ((equal aggr '(16)) (kill-buffer-if-not-modified (current-buffer)))))
(global-set-key (kbd "C-`") 'bh/switch-bury-or-kill-buffer)

;;; keybindings
;;;; bh/describe-key-and-variants
(defun bh/describe-key-and-variants (key)
  "Display key bindings and variants for the next pressed key.

Handles single character keys (e.g., `a'), function keys (e.g.,
F12). Control codes are converted to the corresponding
character before display eg C-d => d."

  (interactive "kPress a key: ")
  (message "")
  (let ((key-str (format "%s" key))) ; 'key' can be a string, vector or ???
    (setq key-str
          (cond ((= (aref key-str 0) 13)
                 "<return>")
                ((= (aref key-str 0) 9)
                 "<tab>")
                ((< (aref key-str 0) ?\ )
                 (concat (char-to-string (+ 64 (aref key-str 0)))
                         (substring key-str 1)))
                ((string= key-str " ") ; <space> is problematic for some reason???
                 "SPC")
                ((> (length key-str) 1) ; eg [f12] -> <f12> but not [
                 (replace-regexp-in-string "\\[" "<" (replace-regexp-in-string "\\]" ">" key-str)))
                (t key-str)))
    (with-output-to-temp-buffer "*Key Variants*"
      (princ (format "Key: %s\n\n" key-str))
      (let ((variants (list key
                            (kbd (concat "S-" key-str))
                            (kbd (concat "C-" key-str))
                            (kbd (concat "S-C-" key-str))
                            (kbd (concat "M-" key-str))
                            (kbd (concat "S-M-" key-str))
                            (kbd (concat "S-C-M-" key-str))
                            (kbd (concat "C-M-" key-str)))))
        (dolist (variant variants)
          (let ((binding (key-binding variant))
                (binding-str "")
                (key-desc (key-description variant)))
            (if binding
                (progn
                  (setq binding-str (format "%s" binding))
                  (if (string-match-p "^C-M-<f[0-9]+>" key-desc)
                      (setq binding-str (concat binding-str ": *** WARNING ***: will be eaten by tty"))))
              (setq binding-str "Not bound"))
            (princ (format "%-20s=>%s\n" key-desc binding-str))))))))
(global-set-key (kbd "C-h C-v")         'bh/describe-key-and-variants)
(global-set-key (kbd "C-c f")           'hi-lock-face-symbol-at-point)
(global-set-key (kbd "C-c u")           'hi-lock-mode)
;;;; c-m- navigation
;;(windmove-default-keybindings)
(global-set-key (kbd "C-M-<up>") 'windmove-up)
(global-set-key (kbd "C-M-<down>") 'windmove-down)
(global-set-key (kbd "C-M-<left>") 'windmove-left)
(global-set-key (kbd "C-M-<right>") 'windmove-right)

;; these enhance windmove:
(defun bh/move-window-to-direction (direction)
  "Move the current window to the specified direction,
swapping it with the window in that direction.
Direction should be one of `left', `right', `up', or `down'."
  (interactive
   (list (completing-read "Direction (left/right/up/down): "
                          '("left" "right" "up" "down"))))
  (let ((buffer (window-buffer))
        (window (selected-window))
        (target-window (window-in-direction direction)))
    (when target-window
      (let ((target-buffer (window-buffer target-window)))
        (set-window-buffer target-window buffer)
        (set-window-buffer window target-buffer)
        (select-window target-window)))))

;; but see the hacks needed elsewhere in this config to get org-mode to behave!!
(global-set-key (kbd "C-M-S-<right>") #'(lambda() (interactive) (bh/move-window-to-direction 'right)))
(global-set-key (kbd "C-M-S-<left>")  #'(lambda() (interactive) (bh/move-window-to-direction 'left)))
(global-set-key (kbd "C-M-S-<up>")    #'(lambda() (interactive) (bh/move-window-to-direction 'up)))
(global-set-key (kbd "C-M-S-<down>")  #'(lambda() (interactive) (bh/move-window-to-direction 'down)))

(global-set-key (kbd "C-M-}")           'enlarge-window-horizontally)
(global-set-key (kbd "C-M-{")           'shrink-window-horizontally)
(global-set-key (kbd "C-M-]")           'enlarge-window)
(global-set-key (kbd "C-M-[")           'shrink-window)
(global-set-key (kbd "C-M-<next>")      'scroll-other-window)
(global-set-key (kbd "C-M-<prior>")     #'(lambda () (interactive)
                                            (scroll-other-window '-)))
;;;; function keys
;; <f1> is a help prefix in emacs -Q - but I use C-h for that
;; I have <f1> mapped to outline-toggle-children in outline-mode;
;; otherwise it's a help prefix same as C-h
;; was (global-set-key (kbd "<f1>")            'scroll-other-window) see C-M-<next>
(global-set-key (kbd "C-<f1>")          'toggle-truncate-lines)
;; reserve <f2> for 2-Column modes like emacs -Q
;; (global-set-key (kbd "<f2>")            'bh/toggle-case-fold)
;; (global-set-key (kbd "S-<f2>")          'bh/toggle-tab-width)
;; (global-set-key (kbd "C-<f2>")          'jump-to-register)
(global-set-key (kbd "<f3>")            'ediff-revision)
(global-set-key (kbd "C-<f3>")          'ediff-buffers)
(global-set-key (kbd "S-<f3>")          'magit-status)
(global-set-key (kbd "<f4>")            'bh/term-and-yank-pwd)
(global-set-key (kbd "C-<f4>")          'bh/toggle-whitespace)
(global-set-key (kbd "S-<f4>")          'bh/pwd-to-clipboard)

;(global-set-key (kbd "<f5>")            'rgrep)
;(global-set-key (kbd "<f5>")            'grep-find)
(global-set-key (kbd "<f5>")            'ripgrep-regexp)
(global-set-key (kbd "C-<f5>")          'consult-locate)
(global-set-key (kbd "<f6>")            'bh/kill-whole-line)
(global-set-key (kbd "C-<f6>")          'bh/yank-whole-line)
(global-set-key (kbd "C-S-<f6>")        'bh/dup-line)
(global-set-key (kbd "<f7>")            'compile)
(global-set-key (kbd "S-<f7>")          #'(lambda ()
                                            (interactive)
                                            (kill-compilation)))
(global-set-key (kbd "C-<f7>")          'next-error)
;(global-set-key (kbd "S-C-<f7>")        'flycheck-next-error)
;(global-set-key (kbd "S-C-<f7>")        #'(lambda () (interactive) (next-error -1)))
;;(global-set-key (kbd "<f8>")            'bookmark-bmenu-list)
(global-set-key (kbd "<f8>")            'consult-bookmark)
(global-set-key (kbd "C-<f8>")          'ibuffer)
(global-set-key (kbd "S-<f8>")          'bh/buffer-menu)
(global-set-key (kbd "<f9>")            'open-rectangle)
(global-set-key (kbd "S-<f9>")          'ispell-word)
;; NB if <f10> is hard-coded to open the menu, it's a gtk problem. Put '[Settings]\ngtk-menu-bar-accel=' in
;; .config/gtk-3.0/settings.ini
(global-set-key (kbd "<f10>")           'kill-rectangle)
(global-set-key (kbd "S-<f10>")         'copy-rectangle-as-kill)
(global-set-key (kbd "C-<f10>")         'recentf-open-files)
(global-set-key (kbd "<f11>")           'yank-rectangle)
(global-set-key (kbd "C-<f11>")         'bh/date-stamp)
(global-set-key (kbd "S-<f11>")         'bh/time-stamp)
(global-set-key (kbd "<f12>")            'other-window)
(global-set-key (kbd "S-<f12>")          'delete-window)
(global-set-key (kbd "C-S-<f12>")        #'(lambda () (interactive) (message "window configuration stored in register n")(window-configuration-to-register 'n)))
(global-set-key (kbd "C-<f12>")          #'(lambda () (interactive) (jump-to-register 'n)))
;; (global-set-key (kbd "M-<f11>")       nil) let window mgr have this for volume-down on achar

;;;;; gdb stuff:
                                        ; maybe use gud-mode-hook?
                                        ;(global-set-key (kbd "M-<f1>")          'gud-next) ; C-c C-n
(global-set-key (kbd "M-<f1>")          'gdb-restore-windows)
                                        ;(global-set-key (kbd "M-<f2>")          'gud-step) ; C-c C-s
(global-set-key (kbd "M-<f2>")          'gdb-many-windows)
                                        ;(global-set-key (kbd "M-<f3>")          'gud-cont) ; C-c C-r
                                        ;(global-set-key (kbd "M-<f4>")          'gud-break); C-c C-b (C-c C-d to delete breakpoint)
                                        ;(global-set-key (kbd "M-<f5>")          'gud-run)
                                        ;(global-set-key (kbd "M-<f6>")          'gud-tbreak)
(global-set-key (kbd "M-<f7>")          'gud-up)
(global-set-key (kbd "M-<f8>")          'gud-down)
                                        ;(global-set-key (kbd "M-<f9>")          'gud-until)
                                        ;(global-set-key (kbd "M-<f10>")         'gud-finish)
                                        ;(global-set-key (kbd "M-<f11>")         #'(lambda()
                                        ;                                            "Set temporary break and go"
                                        ;                                            (interactive)
                                        ;                                            (gud-tbreak t)
                                        ;                                            (gud-cont nil)))
                                        ; (global-set-key (kbd "M-<f12>")         'gud-print)

;;;; c-x keybinds
(define-key ctl-x-map "%"                'bh/paren-match)
(define-key ctl-x-map "="                'what-line)

(global-set-key (kbd "C-x C-b")          'bh/buffer-menu)
(global-set-key (kbd "C-x C-f")          'find-file)

(define-key ctl-x-map "g"                'consult-goto-line)
(define-key ctl-x-map "m"                #'(lambda  ()
                                            "Print manual entry for word at point"
                                            (interactive)
                                            (manual-entry (current-word))))
(define-key ctl-x-map "/"                'query-replace-regexp)
(define-key ctl-x-map "C-v"              'vc-next-action)
;(define-key ctl-x-map "C-w"              'write-file) ; ido-write-file is stupid: in fact, need to use C-x C-w C-w

;;;; misc keybinds
(global-set-key (kbd "C-\\")            'indent-region)
(global-set-key (kbd "M-<delete>")      'flush-lines)
(global-set-key (kbd "C-<delete>")      'keep-lines)
;(global-set-key (kbd "M-s")             'isearch-forward-regexp) ; needed for consult

;; (global-set-key (kbd "S-<iso-lefttab>") 'bh/just-tab) org mode uses this

;; leave these alone - default mapping extends the selection when shifted
;(global-set-key (kbd "M-f")             'forward-whitespace)
;(global-set-key (kbd "M-b")             #'(lambda ()
;                                           (interactive) (forward-whitespace '-1)))
(global-set-key (kbd "C-S-<right>")     'forward-whitespace)
(global-set-key (kbd "C-S-<left>")      #'(lambda ()
                                           (interactive) (forward-whitespace '-1)))

(global-set-key (kbd "S-<insert>")      'bh/insert-x-selection)
(global-set-key (kbd "C-S-v")           'bh/insert-x-clipboard)
(global-set-key (kbd "C-v")             'bh/insert-x-clipboard)

;; (global-set-key [remap dabbrev-expand] 'hippie-expand) ; M-/
;; ;; (global-set-key (kbd "C-\\")            'hippie-expand) ; shadows use in company-try-hard
;; (setq hippie-expand-try-functions-list
;;       (append hippie-expand-try-functions-list
;;               `(try-expand-dabbrev-visible)))

(global-set-key (kbd "C-z")             'undo)
(global-set-key (kbd "C-|")             'repeat-complex-command)
(global-set-key (kbd "M-;")             'bh/toggle-window-split) ; unshifted M-: !!
(global-set-key (kbd "C-<return>")      'mark-defun)

;; Muscle memory fix - I always forget to use 'e' in buffer-menu and the default
;; pushes the selected buffer into the 'next' window - very annoying
(define-key Buffer-menu-mode-map (kbd "<return>") 'Buffer-menu-this-window)

;; zoom in and out:
(global-set-key (kbd "C-+")             'text-scale-adjust)
(global-set-key (kbd "C--")             'text-scale-adjust)
(global-set-key (kbd "C-_")             'text-scale-decrease)
(global-set-key (kbd "C-=")             'text-scale-increase)
(global-set-key (kbd "C-0")             'text-scale-adjust)

;; because scroll-right/left are not very useful and I keep hitting C-<next> by mistake
(global-set-key (kbd "C-<next>")        #'(lambda () (interactive) (scroll-down 1)))
(global-set-key (kbd "C-<prior>")       #'(lambda () (interactive) (scroll-up 1)))

;; shellcheck override:
(global-set-key (kbd "M-\"")            #'(lambda ()
                                            "insert shellcheck clause"
                                            (interactive)
                                            (beginning-of-line)
                                            (insert "# shellcheck disable=SC2034")
                                            (indent-according-to-mode)
                                            (insert "\n")))
(global-set-key (kbd "<menu>")          'menu-bar-open)
(global-set-key (kbd "S-<menu>")        'eval-expression)
(global-set-key (kbd "C-<menu>")        'execute-extended-command)

(global-set-key [backtab]               nil)
                
;;; menu-bar
(setq yank-menu-length 60)
(menu-bar-mode)
;(use-package 'easymenu) ; https://www.emacswiki.org/emacs/EasyMenu - now built-in

;; adapted from https://kitchingroup.cheme.cmu.edu/blog/2014/08/20/Creating-a-dynamic-menu-for-Emacs/
(easy-menu-define xdg-menu global-map "MyFilesMenu"
  '("XDG Recent Files"
    ["Update this menu" (update-my-file-menu t)]
    "---"))

(defun bh/get-menu (label mime-type max-items)
  (let ((command (concat "recentf-gtk -s m -r --mime-type "
                         mime-type
                         "|head -n "
                         max-items)))
    (easy-menu-create-menu
     label
     (mapcar (lambda (x)
               (vector (file-name-nondirectory x)
                       `(lambda () (interactive) (find-file ,x) t)))
             (split-string
              (shell-command-to-string command)
              "\n" t)))))

(defun bh/file-newer-than-file-p (file1 file2)
  "Return t if FILE1 is newer than FILE2 or FILE2 does not exist, nil otherwise."
  (or (not (file-exists-p file2))
      (when (file-exists-p file1)
        (time-less-p (nth 5 (file-attributes file2))
                     (nth 5 (file-attributes file1))))))

(defun update-my-file-menu (&optional force)
  "update the xdg-menu - use a timestamp file to avoid excessive calls.
FORCE over-rides the timestamp file"
  (interactive)
  (when (online?) ;; otherwise any network-mounted files cause a hang!!
    ;; debug: (message (concat "update-my-file-menu: " (format-time-string "%Y%m%d-%H%M%S")))
    (let ((dir (concat (or (getenv "XDG_DATA_HOME") (getenv "HOME")) "/.local/share/"))
          (recently-used-file "recently-used.xbel"))
      (let ((timestamp-file (concat dir "." recently-used-file))
            (recently-used-file (concat dir recently-used-file)))
	(when (or force (bh/file-newer-than-file-p recently-used-file timestamp-file))
          (write-region "" nil timestamp-file nil 'create)
          (let ((max-items "20"))
            (easy-menu-add-item xdg-menu '()
				(bh/get-menu "Text Files"
                                             "text/*"
                                             max-items))
            (easy-menu-add-item xdg-menu '()
				(bh/get-menu "PDF Files"
                                             "application/pdf"
                                             max-items))
            (easy-menu-add-item xdg-menu '()
				(bh/get-menu "Image Files"
                                             "image/*"
                                             max-items))))))))

;; this makes emacs very sluggish without the timestamp file -
;; actually with it, at least on save-buffer. Let's rely on a manual
;; update of the menu!:
;; (add-hook 'menu-bar-update-hook 'update-my-file-menu)
(update-my-file-menu t) ;; force update on startup

(easy-menu-define my-menu global-map "My own menu"
  '("My Stuff"
    ["consult-yank-from-kill-ring" consult-yank-from-kill-ring]
    ["Spell word" ispell-word]
    ["Date stamp 'Fri Sep 17, 2021 11:46'" bh/date-stamp]
    ["Time stamp '20210917114946'" bh/time-stamp]
    ["Recent files" recentf-open-files]
    ["Delete matching lines" flush-lines]
    ["Keep matching lines" keep-lines]
    ["Toggle h/v split" bh/toggle-window-split]
    ["Expand region" er/expand-region]
    ["Shrink region" er/contract-region]
    ["Goto address (URL) at point" goto-address-at-point]
    ["Terminal at cwd" bh/term-and-yank-pwd]
    ["Unformat para" unfill-paragraph]
    ["Highlight symbol" hi-lock-face-symbol-at-point]
    ["Highlight off" hi-lock-mode]
;;    ["Bookmark file" bmkp-bookmark-set-confirm-overwrite]
    ["Org breadcrumbs" (org-display-outline-path nil)]
    ["Balance windows" balance-windows]
    ["Window config to register" window-configuration-to-register]
    ["Jump to register" jump-to-register]
    ("Mark"
     ["word" mark-word]
     ["sentence" er/mark-sentence]
     ["para" mark-paragraph]
     ["sexp" mark-sexp]
     ["defun" mark-defun]
     ["to end" mark-end-of-buffer]
     ["url" er/mark-url]
     ["email" er/mark-email])))
;; (easy-menu-add-item nil '("tools") ["IRC" erc-select t])

;;; misc

(setq display-line-numbers-type nil)

;; use mouse with emacs -nw:
(if (not (window-system))
    (xterm-mouse-mode 1))

;; fix for PureGTK emacs after 20210814:
;;(unless (fboundp 'x-select-font)
;;  (defalias 'x-select-font 'pgtk-popup-font-panel "Pop up the font panel.
;;This function has been overloaded in Nextstep."))

;; see if this helps:
(setq help-window-select t)

(setq find-file-visit-truename t) ;;; follow sym links
(global-auto-revert-mode)

;-------------------------------------
;; Preserve links to files during backup
(setq-default backup-by-copying-when-linked t)

;-------------------------------------
;; Other Emacs variables ...
(column-number-mode 1)

(fset 'yes-or-no-p #'y-or-n-p)           ;replace y-e-s by y

;; Always end a file with a newline
;;(setq require-final-newline t)
;(setq mode-require-final-newline nil)

;; Stop at the end of the file, not just add lines
;(setq next-line-add-newlines nil)

;; Always spaces, never TABs
(setq-default indent-tabs-mode nil)
;(setq default-tab-width 4)

(setq frame-title '("emacs: %b %& %f"))

(setq gdb-many-windows t)

;; (make-variable-buffer-local 'compile-command)
;; (set-default 'compile-command "make -k")

(setq show-paren-style 'expression)
(show-paren-mode)

(server-start)

(delete-selection-mode t)
(setq sentence-end "[.?!][]\"')}]*\\($\\|[ \t]\\)[ \t\n]*")
(setq sentence-end-double-space nil)
(set-scroll-bar-mode 'right)
(tool-bar-mode -1) ; no tool bar, thanks
(transient-mark-mode t)
;;   (load-file "~/.config/emacs/jkb-compr-ccrypt.el")))
;;   (load-file "/usr/share/doc/ccrypt-1.2/jka-compr-ccrypt.el")
;;   (require 'jka-compr-ccrypt "jka-compr-ccrypt.el")))

; error in latest version:
;(require 'ps-ccrypt "ps-ccrypt.el") ; this only exists in .config/emacs so can't use-package it

;; this is built-in: but it works badly with most themes:
;(setq global-hl-line-sticky-flag t)
;(global-hl-line-mode)
;(blink-cursor-mode)

;; take off training wheels:
(put 'narrow-to-region 'disabled nil)
(put 'downcase-region 'disabled nil)

;; Make Text mode the default mode for new buffers.
(setq default-major-mode 'text-mode)

;; Turn on Auto Fill mode automatically in Text mode and related modes.
(add-hook 'text-mode-hook
          (lambda ()
            (auto-fill-mode 1)))

;; I think RedHat puts a symbolic link in, Gentoo needs this:
;; doom has hunspell ... seems OK
(setq-default ispell-program-name "aspell")

;; (setq mouse-yank-at-point t) ; nooooooooo!

;; Don't use dialog boxes - Clicking on an install button for instance
;; makes Emacs spawn dialog boxes from that point on:
(setq use-dialog-box nil)

(setq makefile-warn-suspicious-lines-p nil)

;(autoload 'new-doc (expand-file-name "~/.config/emacs/new-doc") "Create a new fax.
;Use prefix argument to send a letter instead." t)
;(autoload 'ispell-word "ispell" "Check spelling of word at or before point" t)
;(autoload 'ispell-complete-word "ispell" "Complete word at or before point" t)
;(autoload 'ispell-region "ispell" "Check spelling of every word in the region" t)
;(autoload 'ispell-buffer "ispell" "Check spelling of every word in the buffer" t)
(autoload 'calculate-rectangle "calc-rect" "Calculate sum of numbers in rectangle" t)
;(autoload 'calc             "calc.elc"     "Calculator Mode" t nil)
;(autoload 'calc-extensions  "calc-ext.elc" nil nil nil)
;(autoload 'quick-calc       "calc.elc"     "Quick Calculator" t nil)
;(autoload 'calc-grab-region "calc-ext.elc" nil t nil)
;(autoload 'defmath          "calc-ext.elc" nil t t)
;(autoload 'edit-kbd-macro      "macedit.elc" "Edit Keyboard Macro" t nil)
;(autoload 'edit-last-kbd-macro "macedit.elc" "Edit Keyboard Macro" t nil)
;; (autoload 'c++-mode "c++-mode" "Mode for editing c++" t nil)
;(autoload 'c++-mode  "cc-mode" "C++ Editing Mode" t)
;(autoload 'c-mode    "cc-mode" "C Editing Mode" t)
;(autoload 'objc-mode "cc-mode" "Objective-C Editing Mode" t)
;(autoload 'sgml-mode "sgml-mode" "Major mode to edit SGML files." t)
;(autoload 'xml-mode "sgml-mode" "Major mode to edit XML files." t)
;(autoload 'puppet-mode "puppet-mode" "Major mode to edit puppet files." t)
(setq auto-mode-alist
      (append '(
                ("\\.C$"   . c++-mode)
                ("\\.cpp$" . c++-mode)
                ("\\.cc$"  . c++-mode)
                ("\\.c$"   . c-mode)
                ("\\.h$"   . c-mode)
                ("\\.pp$"  . puppet-mode)
                ("\\.m$"   . objc-mode)) auto-mode-alist))

;; Calculator
(global-set-key (kbd "C-c c") 'calc)

(setq mouse-autoselect-window t)
(pixel-scroll-precision-mode t) ;; fails on defvar-1 with emacs-28
(global-goto-address-mode)

;; fix multibyte chars in elfeed - or run toggle-enable-multibyte-characters twice!
(set-language-environment "utf-8")

;; once you start shift-selecting (eg S-C-f) then it stays selecting
(setq shift-select-mode 'permanent)

;;;; electric-indent-mode
;; I don't like electric-indent-mode auto-indenting in org-mode. This seems to be
;; a solution: https://www.philnewton.net/blog/electric-indent-with-org-mode/
(add-hook 'electric-indent-functions
          (lambda (x) (when (eq 'org-mode major-mode) 'no-indent)))

;;; backup
;; don't use <file>~ as backup! - this is from ohai:

;; Emacs writes backup files to `filename~` by default. This is messy,
;; so let's tell it to write them to `~/.emacs.d/bak` instead.
;; If you have an accident, check this directory - you might get lucky.
(setq backup-directory-alist
      `(("." . ,(expand-file-name (concat user-emacs-directory "bak")))))

;;; dired
;; let's revisit these:
;;(put 'dired-find-alternate-file 'disabled nil) ; visiting a file from dired closes the dired buffer
;; dired - For the few times I’m using Dired, I prefer it not spawning
;; an endless amount of buffers. In fact, I’d prefer it using one
;; buffer unless another one is explicitly created, but you can’t have
;; everything:
;;(with-eval-after-load 'dired
;;  (define-key dired-mode-map (kbd "RET") 'dired-find-alternate-file))
(defun bh/dired-open-file ()
  "Open files in Dired with external programs based on their extensions."
  (interactive)
  (let ((file (dired-get-file-for-visit)))
    (cond
     ((or (string-suffix-p ".jpg" file)
          (string-suffix-p ".jpeg" file)
          (string-suffix-p ".png" file)
          (string-suffix-p ".gif" file)
          (string-suffix-p ".bmp" file)   ; Add more formats as needed
          (string-suffix-p ".tiff" file)) ; TIFF support
      (start-process "imv" nil "imv" file))  ; Use imv for images
     ((or (string-suffix-p ".mp4" file)
          (string-suffix-p ".mov" file)
          (string-suffix-p ".mkv" file)
          (string-suffix-p ".avi" file))
      (start-process "mpv" nil "mpv" file))  ; Use mpv for videos
     (t
      (dired-find-file)))))  ; Default behavior for other files

(define-key dired-mode-map (kbd "RET") 'bh/dired-open-file)

;;; src
(defvar bh/checkin-on-save t
  "Provide a way to suppress bh/ensure-in-vc-or-check-in if
desired - where is it used? Forgotten.")
(defun bh/ensure-in-vc-or-check-in ()
  "Automatically checkin file if it's under the control of SRC.
<file>,v can be in the same directory or in the subdirectory RCS
or .src. The idea is to call vc-checkin only for files not for
buffers."
  (interactive)
  (when (and (bound-and-true-p bh/checkin-on-save) buffer-file-name)
    (let ((backend (ignore-errors (vc-responsible-backend (buffer-file-name)))))
      (if (bound-and-true-p backend)
          (when (string= backend 'SRC)
            ;; why does this insist on popping up a window containing the filename?
            ;; (setq log-edit-confirm nil) ; doesn't help
            ;; let's try save-window-excursion
            (save-window-excursion
              (vc-checkin (list buffer-file-name) backend "auto-checkin" nil))
            ;; throws error on nil: (delete-window (get-buffer-window log-edit-files-buf))
            (unless (vc-registered buffer-file-name)
              (vc-register)))))))

;;; themes
(add-to-list 'custom-theme-load-path "~/.config/emacs/themes")
; (load-theme 'dichromacy-bh t) ; 't' means no check

(mapc 'disable-theme custom-enabled-themes)
(setq modus-themes-headings
      (quote ((1 . (1.2))
              (2 . (1.15))
              (3 . (1.1))
              (4 . (1.05)))))
(call-process "dark-mode" nil nil nil "rough")
(if (string=
     "dark\n"
     (with-temp-buffer
       (insert-file-contents "~/.cache/dark-mode")
       (buffer-string)))
    (load-theme 'modus-vivendi)
  (load-theme 'modus-operandi))

;;; modeline
(use-package all-the-icons :defer t)

(use-package doom-modeline
  ;; :defer t ; faster without this!
  :init (doom-modeline-mode 1)
  :custom ((doom-modeline-height 15)))

;; add the breadcrumbs in org mode:
;; https://emacs.stackexchange.com/questions/30894/show-current-org-mode-outline-position-in-modeline
(defun org-which-function ()
  (interactive)
  (when (eq major-mode 'org-mode)
    (mapconcat 'identity (org-get-outline-path t)
               ">")))
(which-function-mode 1)
;;; timestamp messages
;; https://emacs.stackexchange.com/questions/32150/how-to-add-a-timestamp-to-each-entry-in-emacs-messages-buffer
(defun sh/current-time-microseconds ()
  "Return the current time formatted to include microseconds."
  (let* ((nowtime (current-time))
         (now-ms (nth 2 nowtime)))
    (concat (format-time-string "[%Y-%m-%dT%T" nowtime) (format ".%d]" now-ms))))

(defun sh/ad-timestamp-message (FORMAT-STRING &rest args)
  "Advice to run before `message\\=' that prepends a timestamp to each message.

Activate this advice with:
(advice-add \\='message :before \\='sh/ad-timestamp-message)"
  (unless (string-equal FORMAT-STRING "%s%s")
    (let ((deactivate-mark nil)
          (inhibit-read-only t))
      (with-current-buffer "*Messages*"
        (goto-char (point-max))
        (if (not (bolp))
            (newline))
        (insert (sh/current-time-microseconds) " ")))))

(advice-add 'message :before 'sh/ad-timestamp-message)

;;; risky variables
;; https://emacs.stackexchange.com/questions/10983/remember-permission-to-execute-risky-local-variables
;; allow remembering risky variables
;; (defun risky-local-variable-p (sym &optional _ignored) nil)
(advice-add 'risky-local-variable-p :override #'ignore)
;;; final

;; paradox is slow to load and package.el is good enough for the above
;; paradox goes into an infinite 100% cpu loop under puregtk+gccemacs
;(use-package paradox :defer t)
;(setq paradox-github-token '7a71b1bbede3b318a37e875efcb404035da735c6)

;; recentf list only gets saved on graceful exit - so it does not get
;; saved on abnormal exit or when running emacs server. Also, during
;; startup a number of files gets visited (whether you have
;; desktop-save-mode or not) and recentf slows startup a lot. So delay
;; adding the hook to the last possible moment. Note also,
;; recentf-mode is needed by consult-buffer to display files, so make
;; sure this does get executed.

;; (defun bh/recentf-mode-startup ()
;;   (recentf-mode 1)
;;   (add-hook 'find-file-hook #'recentf-save-list))
;; (add-hook 'desktop-after-read-hook #'bh/recentf-mode-startup)
(add-hook 'after-init-hook 'recentf-mode)
(setq recentf-max-saved-items nil) ;; infinite

;;; for emacs
;; Local Variables:
;; outline-minor-mode-highlight: append
;; eval: (outline-minor-mode 1)
;; eval: (outline-hide-sublevels 1)
;; imenu-auto-rescan: nil
;; imenu-sort-function: imenu--sort-by-name
;; imenu-case-fold-search: t
;; eval: (imenu-add-menubar-index)
;; End:
