#!/usr/bin/env bash
# https://gitlab.com/wef/dotfiles/-/blob/master/bin/recentf
export TIME_STAMP="20231217.083048"

# Copyright (C) 2020-2023 Bob Hepple <bob dot hepple at gmail dot com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

prog=$( basename "$0" )
location="${XDG_DATA_HOME:-$HOME/.local/share/recently-used.xbel}"
#columns=$( tput cols )
sort_by="-k2n"
reverse="r"
what="e"
date_print=""
date_fmt="%c"
xmlstarlet_list=( "xmlstarlet" "xml" )
cleanup=""
null=( "cat" )
term_out=""
[[ -t 1 ]] && term_out="set"

TEMP=$( getopt --options 0cd:f:hl:rs:vw: --longoptions null,cleanup,date-print:,date_format:,help,location:,reverse,sort-on:,verbose,what: -- "$@" ) || exit 1
eval set -- "$TEMP"

while [[ "$1" ]]; do
    case "$1" in
        -h|--help)
            cat << EOF
Usage: $prog [OPTIONS]

Print recently used files (if they still exist) from
$location.

Note that the timestamps in the recently-used database may be
different to those on the file itself (eg if something outside gnome
has modified the file).

Use this to get a long list with real file timestamps:

    $prog -0 | xargs -0 ls -l

Note also, that 'visited' time may be meaningless if the filesystem is
mounted with options such as relatime or noatime.

OPTIONS
-0|--null                 terminate filenames with null instead of \\n (for xargs).
                          Implies only the filename is printed.
-c|--cleanup              remove entries of files that no longer exist (prints nothing
                          unless verbose -v option is set)
-d|--date-print <field>   <field> can be a(dded), m(odified) or v(isited). Default is
                          no date printed.
-f|--date_format <fmt>    format to print date. Default is '$date_fmt' - see date(1))
-h|--help                 this help
-l|--location <file>      the recently-used file. Default is
                          '$location'
-r|--reverse              reverse the sort
-s|--sort-on <field>      <field> can be n(ame), a(dded), m(odified) or v(isited).
                          Default is m(odified).
-v|--verbose              be verbose
-w|--what <what>          what to print - a(ll), d(eleted) or e(xisting) filenames.
                          Default is 'existing'.
EOF
            exit 0
            ;;
        -0|--null)
            null=( 'tr' '\n' '\0' )
            ;;
        -w|--what)
            shift
            what="$1"
            ;;
        -s|--sort)
            shift
            case "$1" in
                a*) sort_by="-k1n" ;;
                m*) sort_by="-k2n" ;;
                v*) sort_by="-k3n" ;;
                *)  sort_by="-k4"  ;;
            esac
            ;;
        -d|--date-print)
            shift
            date_print="$1"
            ;;
        -f|--date_format)
            shift
            date_fmt="$1"
            ;;
        -r|--reverse)
            reverse=""
            ;;
        -l|--location)
            shift
            location="$1"
            ;;
        -c|--cleanup)
            cleanup="set"
            ;;
        -v|--verbose)
            verbose="set"
            ;;
        --)
            :
            ;;
    esac
    shift
done

[[ "${null[0]}" == "tr" ]] && {
    if [[ "$date_print" ]]; then
        echo "$prog: warning: ignoring -d|--date-print option as -0|--null is in force" >&2
        date_print=""
    fi
}

url2file() {
    url="$1"
    local without_protocol="${url#file://}"
    # Replace percent-encoded characters with their ASCII equivalents
    printf "%b" "$(echo "$without_protocol" | sed 's/+/ /g;s/%/\\x/g')" |
    sed -E 's/&apos;/'\''/g; s/&quot;/"/g; s/&amp;/\&/g; s/&lt;/</g; s/&gt;/>/g; s/&nbsp;/ /g; s/&iexcl;/¡/g; s/&cent;/¢/g; s/&pound;/£/g; s/&curren;/¤/g; s/&yen;/¥/g; s/&euro;/\u20ac/g; s/&copy;/©/g; s/&reg;/®/g; s/&trade;/\u2122/g; s/&deg;/°/g; s/&plusmn;/±/g; s/&sup2;/²/g; s/&sup3;/³/g; s/&micro;/µ/g; s/&para;/¶/g; s/&middot;/·/g; s/&cedil;/ç/g; s/&szlig;/ß/g; s/&agrave;/à/g; s/&aacute;/á/g; s/&acirc;/â/g; s/&atilde;/ã/g; s/&auml;/ä/g; s/&aring;/å/g; s/&aelig;/æ/g; s/&ccedil;/ç/g; s/&egrave;/è/g; s/&eacute;/é/g; s/&ecirc;/ê/g; s/&euml;/ë/g; s/&igrave;/ì/g; s/&iacute;/í/g; s/&icirc;/î/g; s/&iuml;/ï/g; s/&eth;/ð/g; s/&ntilde;/ñ/g; s/&ograve;/ò/g; s/&oacute;/ó/g; s/&ocirc;/ô/g; s/&otilde;/õ/g; s/&ouml;/ö/g; s/&divide;/÷/g; s/&oslash;/ø/g; s/&ugrave;/ù/g; s/&uacute;/ú/g; s/&ucirc;/û/g; s/&uuml;/ü/g; s/&yacute;/ý/g; s/&thorn;/þ/g; s/&yuml;/ÿ/g;'
}
    
xmlstarlet=""
for x in "${xmlstarlet_list[@]}"; do
    type "$x" &> /dev/null && xmlstarlet="$x" && break
done
[[ "$xmlstarlet" ]] || {
    echo "$prog: can't find ${xmlstarlet_list[*]} - please install xmlstarlet" >&2
    exit 1
}

"$xmlstarlet" val "$location" > /dev/null 2>&1 || {
    echo "$prog: '$location' is not a valid XML file!" >&2
    exit 1
}

if [[ "$cleanup" ]]; then
    tmp=$( mktemp )
    trap 'rm "$tmp"' EXIT
    cp "$location" "$tmp"
    "$xmlstarlet" sel --template --match "//bookmark" -v '@href' --nl "$location" |
    while read -r href; do     
        filename="$(url2file "$href")"  
        # xmlstarlet converts ' to &apos; etc - while this is valid XML, who knows
        # if it's accepted everywhere?
        # Instead, use bogus line-editing utilities which rely on the XML being
        # formatted in a regular way:
        [[ -e "$filename" ]] || {
            [[ "$verbose" ]] && echo "Removing '$filename' from '$location'"
            pattern="${href////\\/}"
            sed -i "/<bookmark href=\"$pattern\"/,/<\/bookmark>/d" "$tmp"
        }
    done
    mv -f "$location" "$location".bak    
    cp -f "$tmp" "$location"
    exit $?
fi

convert_date() {
    date --utc -d "$1" +%s
}

"$xmlstarlet" sel --template --match \
              "//bookmark" -v 'concat(@added, " ", @modified, " ", @visited, " ", @href)'\
              --nl "$location" |
while read -r added modified visited href; do
    filename="$( url2file "$href" )"
    if [[ "$sort_by" == "-k4" && -z "$date_print" ]]; then
        # sort by name and don't print dates - we won't need the dates; saves a bit of time:
        added=1
        modified=1
        visited=1
    else
        added=$( convert_date "$added" )
        modified=$( convert_date "$modified" )
        visited=$( convert_date "$visited" )
    fi
    print_it=""
    exists=""
    if [[ "$what" =~ a.* ]]; then
        print_it="set"
    else
        [[  -e "$filename" ]] && exists="set"
        if [[ "$what" =~ d.* ]]; then
            [[ "$exists" ]] || print_it="set"
        else # $what =~ e.*
            [[ "$exists" ]] && print_it="set"
        fi
    fi
    if [[ "$exists" ]]; then
        filename=$( readlink -e "$filename" )
    fi
    if [[ "$term_out" && "$filename" =~ ^$HOME(.*) ]]; then
        filename="~${filename#"$HOME"}"
    fi

    [[ "$print_it" ]] && printf "%s %s %s %s\n" "$added" "$modified" "$visited" "$filename"
done |
sort $sort_by$reverse |
while read -r added modified visited filename; do

    case "$date_print" in
        m*) date="$modified" ;;
        v*) date="$visited"  ;;
        a*) date="$added"    ;;
        *)  date=""          ;;
    esac

    if [[ "$date" ]]; then
        printf "%s %s\n" "$( date -d "@$date" "+$date_fmt" )" "$filename"
    else
        printf "%s\n" "$filename"
    fi |
    "${null[@]}"
done

# Local Variables:
# mode: shell-script
# time-stamp-pattern: "4/TIME_STAMP=\"%:y%02m%02d.%02H%02M%02S\""
# eval: (add-hook 'before-save-hook 'time-stamp)
# End:
