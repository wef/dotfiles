#!/usr/bin/env bash
# https://gitlab.com/wef/dotfiles/-/blob/master/bin/i3-pip
# shellcheck disable=SC2034
TIME_STAMP="20240901.181208"

# Copyright (C) 2020-2023 Bob Hepple <bob dot hepple at gmail dot com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

PROG=$( basename "$0" )
wm="i3"
msg="i3-msg"
[[ "$SWAYSOCK" ]] && {
    wm="sway"
    msg="swaymsg"
}

waitfor=
corner="bottom-right"
while [[ "$1" ]]; do
    case "$1" in
        -h|--help)
            echo "Usage: $PROG [command]"
            echo "Runs [command] and sets the resulting window to Picture-in-Picture mode"
            echo "ie small, bottom right, no borders and sticky."
            echo "If no command is given, PIP the focused window."
            echo
            echo "-w,--waitfor id    wait for <id> rather than the first item in <command>"
            exit 0
            ;;
        -w|--waitfor)
            shift
            waitfor="--waitfor $1"
            shift
            ;;
        -c|--corner)
            shift
            corner="$1"
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            break
            ;;
    esac
done

id=
[[ "$1" ]] && {
    # shellcheck disable=SC2086
    id=$( i3-toolwait --verbose $waitfor -- "$@" ) || exit $? 
}

# the focus may change during these operations, so tie it down to the current one
if [[ "$id" ]]; then
    json=$( $msg -t get_tree | jq ".. | select(.type?) | select(.id==$id)" )
else
    json=$( $msg -t get_tree | jq ".. | select(.type?) | select(.focused==true)" )
    id=$( echo "$json" | jq '.id' )
fi

[[ "$id" ]] && {
    $msg "[con_id=$id] floating enable"
    sleep 1 # mpv (at least) needs this
    set -x
    width=$( echo "$json" | jq '.rect.width' )
    height=$( echo "$json" | jq '.rect.height' )
    (( height > 0 )) || exit 1
    
    std_height=300
    calc_width=$(( std_height * width / height ))

    $msg "[con_id=$id] resize set width $calc_width px height $std_height px, border none, sticky enable, focus" &&
        i3-move-to --id "$id" "$corner"
}

# Local Variables:
# mode: shell-script
# time-stamp-pattern: "4/TIME_STAMP=\"%:y%02m%02d.%02H%02M%02S\""
# eval: (add-hook 'before-save-hook 'time-stamp)
# End:

