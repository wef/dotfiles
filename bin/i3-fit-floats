#!/usr/bin/env bash
# https://gitlab.com/wef/dotfiles/-/blob/master/bin/i3-fit-floats
# shellcheck disable=SC2034
TIME_STAMP="20241117.174049"

# Copyright (C) 2020-2021 Bob Hepple <bob dot hepple at gmail dot com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

initialise() {
    PROG=$(basename "$0")
    VERSION="$TIME_STAMP"
    ARGUMENTS=""
    USAGE="Arrange floating/tiling/scratchpad windows in i3/sway.

Operations are performed in this order (as requested):

--unfloat
--float
--toggle
--scratchpad
--fit

NB this is untested in multiple monitor setups.

Suggested keys:

# fits all visible floaters:
bindsym    \$mod+backslash   exec $PROG --fit         --x-origin=20 --y-origin=20 --padx=20 --pady=20 -v

# if there are no floaters, bring them all from scratchpad; else send all floaters to scratchpad:
bindsym \$mod+\$c+backslash   exec $PROG --toggle      --x-origin=20 --y-origin=20 --padx=20 --pady=20 -v

# send visible floating windows to scratchpad:
bindsym \$mod+\$s+backslash   exec $PROG -S --no-focus --x-origin=20 --y-origin=20 --padx=20 --pady=20 -v

# unfloat all visible windows:
bindsym \$mod+\$c+\$s+backslash   exec $PROG --unfloat

# send all but the focused window to scratchpad (like C-x 1 in emacs):
bindsym \$mod+f4 exec $PROG --no-focus --float --scratchpad -v
"

    NEW_ARGS=( )

    ARGS="
ARGP_DELETE=quiet
ARGP_VERSION=$VERSION
ARGP_PROG=$PROG
##############################################################   
#OPTIONS:
#name=default   sname arg       type range   description
################################################################   
ALL=''          a     ''        b    ''      operate on all windows (not just visible - includes scratchpad)
PADX='0'        ''    padx      i    '0-'    pad windows right
PADY='0'        ''    pady      i    '0-'    pad windows below
SIZEX=''        x     sizex     i    '0-'    resize floaters
SIZEY=''        y     sizey     i    '0-'    resize floaters
X_ORIGIN='0'    ''    xorg      i    '0-'    offset in x
Y_ORIGIN='0'    ''    yorg      i    '0-'    offset in y
FIT=''          ''    ''        b    ''      fit the windows
SHELF=''        s     ''        b    ''      use simple shelf tiling instead of 'fill_windows' tiling
SCRATCHPAD=''   S     ''        b    ''      send visible (or --all) floating windows to scratchpad
NO_FOCUS=''     f     ''        b    ''      exclude the focused window [-S, -F, -t, -x, -y and -u]
TOGGLE=''       t     ''        b    ''      if there are no floaters, bring all from scratchpad and fit; else send all floaters to scratchpad
UNFLOAT=''      u     ''        b    ''      change floating windows to normal windows
FLOAT=''        F     ''        b    ''      change tiled windows to floaters
BORDER='normal' b     border    s    ''      border (normal,none,csd,<pixel>) pixel is a number
##############################################################   
ARGP_ARGS=[--] $ARGUMENTS
ARGP_SHORT=$SHORT_DESC
ARGP_USAGE=$USAGE"

    exec 4>&1
    eval "$(echo "$ARGS" | argp.sh "$@" 3>&1 1>&4 || echo exit $? )"
    exec 4>&-

    NEW_ARGS=( "$@" )

    return 0
}

verbose() {
    [[ "$VERBOSE" ]] && echo "$PROG: $*" >&2
}

do_msg() {
    verbose "$*"
    $msg -q "$*"
}

# assume we're filling from top left, windows appear in height order
add_node() {
    x=$(( $1 ))
    y=$(( $2  ))
    for ((i=0; i<num_nodes; i++)); do
        # make sure it's unique:
        if (( x == node_x[i] )) && (( y == node_y[i] )); then
            # corner case of covering up an existing node
            node_x[i]=-1
            return
        fi
    done

    if (( x < workspace_width )) && (( y < workspace_height )); then
        node_x[i]=$x
        node_y[i]=$y
        num_nodes=$(( num_nodes + 1 ))
    fi
}

place_window() {
    local index=$1 i
    verbose "trying to place window[$index] id ${win_id[index]} w=${win_width[index]} h=${win_height[index]} on $num_nodes nodes"
    for ((i=0; i<num_nodes; i++)); do
        if (( node_x[i] == -1 )); then
            continue
        fi
        
        if ((  win_width[index]  + node_x[i] < workspace_width  )) && 
            (( win_height[index] + node_y[i] < workspace_height )); then

            verbose "success! placed window[$index] on node[$i]"

            # Success!
            win_x[index]=${node_x[i]}
            win_y[index]=${node_y[i]}
            node_x[i]=-1 # used
            # order is important here - top right, bottom left, bottom right
            add_node $(( win_x[index] + win_width[index] + PADX )) "${win_y[index]}"
            add_node "${win_x[index]}" $(( win_y[index] + win_height[index] + PADY ))
            add_node $(( win_x[index] + win_width[index] + PADX )) $(( win_y[index] + win_height[index] + PADY ))
            win_placed[index]="true"
            return 0
        fi
    done

    # this one fits nowhere - start again
    return 1
}

fill_windows() {
    # slurp all windows into arrays:
    local index=0 id width height
    
    while read -r id width height _; do
        if (( width + PADX > workspace_width - X_ORIGIN )) || (( height + PADY > workspace_height - Y_ORIGIN )); then
            echo "$PROG: ignoring window $id as it's too large to ever fit" >&2
            continue
        fi
        verbose "got window $id $width $height"
        win_id[index]=$id
        win_width[index]=$width
        win_height[index]=$height
        win_placed[index]=""
        index=$(( index + 1 ))
    done
    num_windows=$index

    verbose "got $num_windows windows"

    # Loop over all the rectangles, fitting them in somewhere
    local finished="false"
    local safety=0
    while [[ $finished == "false" ]] && (( safety < 50 )); do
        node_x[0]=$X_ORIGIN # '-1' means a used node
        node_y[0]=$Y_ORIGIN
        num_nodes=1
        safety=$(( safety + 1 ))

        for (( index=0; index < num_windows; index++ )); do
            if [[ ${win_placed[index]} != "true" ]]; then
                place_window "$index"
            fi
        done
        
        finished="true"
        for ((index=0; index < num_windows; index++)); do
            if [[ ${win_placed[index]} != "true" ]]; then
                finished="false"
                verbose "starting again at new origin to avoid superimposing windows"
                X_ORIGIN=$(( X_ORIGIN + 20 ))
                Y_ORIGIN=$(( Y_ORIGIN + 20 ))
                break
            fi
        done
    done
    
    # output results
    for ((index=0; index < num_windows; index++)); do
        if [[ ${win_placed[index]} == "true" ]]; then
            echo "${win_id[index]} ${win_x[index]} ${win_y[index]}"
        fi
    done
}

simple_shelf_pack() {
    xPos=$X_ORIGIN
    yPos=$Y_ORIGIN
    largestHThisRow=$Y_ORIGIN
    
    # slurp all rectangle into arrays:
    index=0
    while read -r id width height _; do
        if (( width + PADX > workspace_width - X_ORIGIN )) || (( height + PADY > workspace_height - Y_ORIGIN )); then
            echo "$PROG: ignoring window $id as it's too large to ever fit" >&2
            continue
        fi
        verbose "got window $id $width $height"
        win_id[index]=$id
        win_width[index]=$width
        win_height[index]=$height
        win_placed[index]=""
        index=$(( index + 1 ))
    done
    num_windows=$index
    
    # Loop over all the rectangles, fitting them in somewhere
    for ((index=0; index < num_windows; index++)); do
        # If this rectangle will go past the width of the image
        # Then loop around to next row, using the largest height from the previous row
        if (( (xPos + win_width[index]) + PADX > workspace_width - X_ORIGIN )); then
            yPos=$(( yPos + largestHThisRow + PADY ))
            xPos=$X_ORIGIN
            largestHThisRow=$Y_ORIGIN
        fi

        # If we go off the bottom edge of the image, then start again
        if (( (yPos + win_height[index]) > workspace_height )); then
            xPos=$X_ORIGIN
            yPos=$Y_ORIGIN
        fi

        # This is the position of the rectangle
        win_x[index]=$xPos
        win_y[index]=$yPos

        # Move along to the next spot in the row
        xPos=$(( xPos + win_width[index] + PADX ))
        
        # Just saving the largest height in the new row
        if (( win_height[index] + PADY > largestHThisRow )); then
            largestHThisRow=$(( win_height[index] + PADY ))
        fi
        
        # Success!
        win_placed[index]="true"
    done

    for ((index=0; index < num_windows; index++)); do
        if [[ ${win_placed[index]} == "true" ]]; then
            echo "${win_id[index]} ${win_x[index]} ${win_y[index]}"
        fi
    done
}

wm="i3"
msg="i3-msg"
type_field=".floating"
[[ "$SWAYSOCK" ]] && {
    wm="sway"
    msg="swaymsg"
    type_field=".type"
}

initialise "$@" && set -- "${NEW_ARGS[@]:-}"

[[ "$BORDER" =~ [0-9] ]] && BORDER="pixel $BORDER"
           
read -r focused focused_type < <(
    $msg -t get_tree |
    jq -j "..| select(.type?) | select(.focused==true) | .id, \" \", $type_field" )

verbose "focused, focused_type: $focused $focused_type"
[[ "$SWAYSOCK" ]] || {
    # i3wm uses a different field and values - rationalise to sway's for convenience.
    [[ "$focused_type" =~ (auto|user)_on ]] && focused_type="floating_con"
}

if [[ "$SWAYSOCK" ]]; then
    Q_floats='.. | select(.type?) | select(.type=="floating_con")'
    Q_tiled='..  | select(.type?) | select(.type=="con")'
    Q_visible='  | select(.visible==true)'
else
    Q_floats='.. | select(.type?) | select(.floating == "auto_on" or .floating == "user_on")'
    Q_tiled='..  | select(.type?) | select(.floating == "auto_off" or .floating == "user_off")'
    Q_visible='  | select(.output != "__i3")'
fi
Q_params=' | .id, " ", .rect.width, " ", .rect.height + .deco_rect.height, "\n"'

if [[ "$FLOAT" ]]; then
    QUERY="$Q_tiled"
    [[ "$ALL" ]] || QUERY+="$Q_visible" # select only visible windows
    QUERY+="$Q_params"
    verbose "float: QUERY=$QUERY"
    $msg -t get_tree | jq -j "$QUERY" | while read -r id rest; do
        if [[ "$NO_FOCUS" == "" ]] || [[ $id != "$focused" ]]; then
            do_msg "[con_id=$id] focus; floating enable; border $BORDER"
        fi
    done
fi

QUERY="$Q_floats"
[[ "$ALL" ]] || QUERY+="$Q_visible" # select only visible windows
QUERY+="$Q_params"

if [[ "$UNFLOAT" ]]; then
    verbose "unfloat: QUERY=$QUERY"
    $msg -t get_tree | jq -j "$QUERY" | while read -r id rest; do
        if [[ "$NO_FOCUS" == "" ]] || [[ $id != "$focused" ]]; then
            do_msg "[con_id=$id] focus; floating disable; border $BORDER"
        fi
    done
fi

if [[ "$TOGGLE" ]]; then
    # count number of visible floating windows
    verbose "toggle: QUERY=$QUERY"
    num_floaters=0
    while read -r id _; do
        num_floaters=$(( num_floaters + 1 ))
    done < <( $msg -t get_tree | jq -j "$Q_floats$Q_visible$Q_params" )
    verbose "num of visible floaters = $num_floaters"
    
    if (( num_floaters > 0 )); then
        SCRATCHPAD="true"
        # so that it doesn't get restored:
        [[ "$id" == "$focused" ]] && [[ "$focused_type" == "floating_con" ]] && focused=""
    else # there are no floaters:
        unset SCRATCHPAD
        QUERY="$Q_floats$Q_params" # move all windows to current w/s including scratchpad
        FIT="set" # force the fit
    fi
fi

if [[ "$SCRATCHPAD" ]]; then
    # send windows to scratchpad - no point in doing a --fit
    verbose "scratchpad: QUERY=$QUERY"
    while read -r id _; do
        if [[ "$NO_FOCUS" == "" ]] || [[ $id != "$focused" ]]; then
            # so that it doesn't get restored:
            [[ "$id" == "$focused" ]] && [[ "$focused_type" == "floating_con" ]] && focused=""
            do_msg "[con_id=$id] focus; move window to scratchpad"
        fi
    done < <( $msg -t get_tree | jq -j "$QUERY" )
    FIT=""
fi

if [[ "$SIZEX$SIZEY" ]]; then
    verbose "sizex/y: QUERY=$QUERY"
    while read -r id _; do
        if [[ "$NO_FOCUS" == "" ]] || [[ $id != "$focused" ]]; then
            if [[ "$SIZEX" && "$SIZEY" ]]; then
                do_msg "[con_id=$id] focus; resize set width $SIZEX height $SIZEY; border $BORDER"
            else
                [[ "$SIZEX" ]] && do_msg "[con_id=$id] focus; resize set width  $SIZEX; border $BORDER"
                [[ "$SIZEY" ]] && do_msg "[con_id=$id] focus; resize set height $SIZEY; border $BORDER"
            fi
        fi
    done < <( $msg -t get_tree | jq -j "$QUERY" )
fi

if [[ "$FIT" ]]; then
    # find smallest "workspace" height
    workspace_height=$(
        min=0
        $msg -t get_tree | jq '..| select(.type?)|select(.type=="workspace")|.rect.height' | (
            while read -r h; do
                if (( min == 0 )) || (( h < min )); then
                    min=$h
                fi
            done
            echo "$min"
        )
    )

    # the width of any "workspace" should do (but on i3 the first is 0 for some reason):
    workspace_width=$(
        $msg -t get_tree | jq '..| select(.type?)|select(.type=="workspace")|.rect.width' |
        tail -n 1
    )

    verbose "workspace is ${workspace_width}x${workspace_height}"

    tile_function="fill_windows"
    # simple_shelf_pack:
    [[ "$SHELF" ]] && tile_function="simple_shelf_pack"

    verbose "fit: QUERY=$QUERY"
    $msg -t get_tree |
    jq -j "$QUERY" |
    sort -k3nr | # sort by height, largest first
    $tile_function |
    while read -r id x y; do
        if [[ "$NO_FOCUS" == "" ]] || [[ $id != "$focused" ]]; then
            do_msg "[con_id=$id] move window to workspace current; [con_id=$id] focus; move position $x $y"
        fi
    done
fi

# restore focus:
[[ "$focused" ]] && {
    do_msg "[con_id=$focused] focus"
}

# Local Variables:
# mode: shell-script
# time-stamp-pattern: "4/TIME_STAMP=\"%:y%02m%02d.%02H%02M%02S\""
# eval: (add-hook 'before-save-hook 'time-stamp)
# End:

