#!/bin/bash
# https://gitlab.com/wef/dotfiles/bin/gallery.cgi

TIME_STAMP="20250125.123313"

# Copyright (C) 2024-2025 Bob Hepple < bob dot hepple at gmail dot com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

: << "EOF"

This simple script serves photos when installed in a web server's cgi
folder. It does not write anything to the photo directory so it can
be made read-only. It is intended for local-only use for conveniently
browsing the photos - it has not been audited for use on the WWW.

The photos are assumed to be arranged in a tree. Each directory in the
tree is to contain a plain text file 'title.ttl' with a one line
description of that level eg "Day out at Movieworld". That title is
displayed at the top of the screen with the directory path.

eg

photos
photos/title.ttl # contains "Photo Viewer"
photos/2024
photos/2024/title.ttl # contains "Australia, Singapore, France"
photos/2024/20240123
photos/2024/20240123/title.ttl # contains "Day out at Movieworld"
photos/2024/20240123/IMG01.jpg
photos/2024/20240123/IMG02.jpg
photos/2024/20240123/VID01.mp4
photos/2024/20240123/thumbnails/IMG01.jpg
photos/viewer/* # navigation buttons up.gif, next.gif etc
photos/cgi-bin # possible global_location of this file depending on your web server
../etc

For every sub-directory, the appropriate title.ttl file is displayed
in a menu.

If a directory contains JPEG or movie files, thumbnails are displayed
(from a 'thumbnails' directory which should be pre-built - see the
script make-thumbnails in the same place you got this).

If a thumbnail is clicked, the screen displays just that photo and any
meta data. If the meta data contains latitude and longitude data, a
link to that global_location in google maps is provided.

If the photo is clicked, the photo is displayed full-screen. If
clicked again, the photo is displayed 1:1.

At all levels (except 1:1 mode), navigation buttons are provided. In
1:1 mode ie without navigation buttons, use Alt-Left to return.

Also the Left, Right and Up keys can be used to navigate.

Searching of the title.ttl files is supported - it's not very fast at
the moment.

Installation:

Put this script in a cgi-bin directory for the web server of your
choice and make it executable.

Create a 'viewer' directory with icons for:

bg.gif first.gif last_disabled.gif movie.gif next.gif previous.gif
first_disabled.gif index.gif last.gif next_disabled.gif
previous_disabled.gif up.gif

Simplest startup (assuming nothing else is listening on port 80):

Start a thttpd web server server with this:

sudo thttpd -d <path to photos> -c '**.cgi' -nos
then http://localhost/cgi-bin/gallery.cgi

Or apache httpd:
In /etc/apache/httpd.conf:
Include /etc/apache/extra/photos.conf

In /etc/apache/extra/photos.conf:
<VirtualHost *:80>
    ServerName 192.168.0.19
    DocumentRoot <path to photos>/photos
    <Directory "<path to photos>/photos">
        AllowOverride None
    </Directory>

    <Directory "/export">
        Options +Indexes +FollowSymLinks
        AllowOverride None
        Require all granted
    </Directory>

    ScriptAlias /cgi-bin/ "<path to photos>/photos/cgi-bin/"

    <Directory "<path to photos>/photos/cgi-bin/">
        AllowOverride None
        Options +ExecCGI +Indexes +FollowSymLinks
    </Directory>
</VirtualHost>

To deploy on aloo:
cp ~/bin/gallery.cgi <path to photos>/photos/cgi-bin/

# emacs will auto-deploy this script on achar (see last few lines)
EOF

# to debug:
[[ "$HOSTNAME" == "achar" ]] && {
    exec 2>/tmp/junk
    set -x
    pwd
    ls
}

thumbnail_height="180"
title_file="title.ttl"
icons="/viewer"
excl_dirs="viewer|cgi-bin|faces|thumbnails"
images="jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF"
videos="mov|mp4|avi|mkv|MOV|MP4|AVI|MKV"
video_thumb="mvi.jpg"

exec_search() {
    global_uri="$1"
    search_term="$2"
    root=$( urldecode "$3" )
    [[ "$root" ]] || root="."
    output_normal_http_header

    # there's no point searching 'cluster' (unless I add titles one day):
    find "$root" -name cluster -prune -o -name title.ttl -exec grep -i "$search_term" {} /dev/null \; |
    sort | {
        echo "<hr>Query results for '$search_term':<ul>"
        while IFS=: read -r file desc; do
            file="${file#./}"
            file="${file%/title.ttl}"
            echo "<li><a href=\"$global_uri?$file\">$file: $desc</a></li>"
        done
        echo "</ul>"
    }
    output_trailer
}

output_search_html() {
    cat << EOF
<form action="gallery.cgi" method="get">
<label for="search">Search:</label>
<input type="text" id="search" name="search" required>
<input type="hidden" name="root" value="${global_parent:+$global_parent/}$global_file">
<button type="submit">Search</button>
</form>
EOF
}

get_adjacent() {
    this_file="$1"

    global_first=""
    global_last=""
    global_prev=""
    global_next=""
    matched=""
    for i in *; do
        [[ "$i" =~ \.$video_thumb$ ]] && continue
        # we're only interested in images, videos and directories:
        if [[ "$i" =~ \.($images|$videos)$ || -d "$i" ]]; then

            if [[ -d "$i" ]]; then
                [[ "$i" =~ ($excl_dirs) ]] && continue
                if [[ "$HOSTNAME" == "aloo" ]]; then
                    desc=$( xxd -p < "$i/$title_file" 2> /dev/null)
                    if [[ "$desc" =~ (53616c6c79|4572696b61) ]]; then
                        continue
                    fi
                fi
            fi

            [[ "$global_first" ]] || global_first="$i"
            if [[ "$i" == "$this_file" ]]; then
                global_prev="$global_last"
                matched="set"
            else
                [[ "$matched" ]] && {
                    [[ "$global_next" ]] || global_next="$i"
                }
            fi
            global_last="$i"
        fi
    done
    [[ "$global_first" == "$global_file" ]] && global_first=""
    [[ "$global_last" == "$global_file" ]] && global_last=""
    [[ "$global_prev" == "$global_file" ]] && global_prev=""
    [[ "$global_next" == "$global_file" ]] && global_next=""
}

navigation_buttons() {
    echo "<p><table><tr><td>"
    if [[ "$global_first" ]]; then
        echo "<a id=\"global_first\" href=\"$global_uri?${global_parent:+${global_parent}/}$global_first\"><img src=\"$icons/first.gif\" ALT=\"First\"></a>"
    else
        echo "<img src=\"$icons/first_disabled.gif\" ALT=\"First\">"
    fi

    echo "</td><td>"
    if [[ "$global_prev" ]]; then
        # ${global_parent:+${global_parent}/} ... if $global_parent is non-blank, add a '/':
        echo "<a id=\"global_prev\" href=\"$global_uri?${global_parent:+${global_parent}/}$global_prev\"><img src=\"$icons/previous.gif\" ALT=\"Previous\"></a>"
    else
        echo "<img src=\"$icons/previous_disabled.gif\" ALT=\"Previous\">"
    fi

    echo "</td><td>"
    echo "<a id=\"up\" href=\"$global_uri?${global_parent}\"><img src=\"$icons/up.gif\" ALT=\"Up\"></a>"

    echo "</td><td>"
    if [[ "$global_next" ]]; then
        echo "<a id=\"global_next\" href=\"$global_uri?${global_parent:+${global_parent}/}$global_next\"><img src=\"$icons/next.gif\" ALT=\"Next\"></a>"
    else
        echo "<img src=\"$icons/next_disabled.gif\" ALT=\"Next\">"
    fi

    echo "</td><td>"
    if [[ "$global_last" ]]; then
        echo "<a id=\"global_last\" href=\"$global_uri?${global_parent:+${global_parent}/}$global_last\"><img src=\"$icons/last.gif\" ALT=\"Last\"></a>"
    else
        echo "<img src=\"$icons/last_disabled.gif\" ALT=\"Last\">"
    fi

    echo "</td>"
    echo -n "<td style=\"text-align: center; white-space: nowrap;\"><a href=\"$global_uri\"> photos </a>"
    [[ "$global_parent" ]] && {
        echo -n " -> <a href=\"$global_uri?$global_parent\"> $global_parent </a>"
    }
    echo " -> $global_file</td><td>"
    output_search_html
    echo "</td></tr></table><hr>"
}

urlencode() {
    local string="${1}"
    local strlen=${#string}
    local encoded=""
    local pos c o

    for (( pos=0 ; pos<strlen ; pos++ )); do
        c=${string:$pos:1}
        case "$c" in
            [-_.~a-zA-Z0-9] ) o="${c}" ;;
            * )               printf -v o '%%%02x' "'$c"
        esac
        encoded+="${o}"
    done
    echo "${encoded}"
}

urldecode() {
    local url_encoded="${1//+/ }" # replace '+' with ' '
    printf '%b' "${url_encoded//%/\\x}"
}

get_coords() {
    awk -F ':' -e $'
        function print_coords(input) {
            # lat/long can be blank or "? ?"
            gsub(/^[ ?\t]+|[ ?\t]+$/, "", input)
            if (input != "") {
                   split(input, a, " ")
                   ind=( a[1] == "S" || a[1] == "W" ) ? "-" : "+"
                   printf("%c%.9f ", ind, (a[2]+0.0)+(a[3]+0.0)/60.0+(a[4]+0.0)/3600.0)
               }
        }
        $1 ~ /^GPS Latitude/  { print_coords($2) }
        $1 ~ /^GPS Longitude/ { print_coords($2) }
        '
}

output_fullscreen_html() {
    pushd "$global_parent" > /dev/null || exit 1
    output_fullscreen_http_header

    cat << EOF
<img src="/$global_location" alt="Your Image">
</body>
</html>
EOF
}

output_file_html() {
    output_normal_http_header
    desc=""
    [[ "$global_location" =~ \.($images)$ ]] &&
        desc=$( jhead "$global_location" 2>/dev/null |awk '/^Comment/ {$1="";$2=""; print; exit}' )
    output_heading " - $global_location: $desc"
    pushd "$global_parent" > /dev/null || exit 1
    navigation_buttons "$global_uri" "$global_location" "$global_first" "$global_prev" "$global_next" "$global_last" "$global_parent"
    echo "<p><table class=\"constrained-table\"><tr><td valign=\"top\">"
    path="$(urlencode "$global_location")"
    path="${path//%2f//}"
    if [[ "$global_location" =~ \.($images)$ ]]; then
        echo "<a href=\"?$path&fullscreen=1\"><img src=\"/$global_location\" class=\"scaled\" alt=\"Your Image\"></a>"
    else
        thumb="/$global_parent/thumbnails/${global_file%.*}.$video_thumb"
        echo "<a href=\"/$global_location\"><img src=\"$thumb\" alt=\"$thumb\"></a>"
    fi
    echo "</td><td valign=top>"
    info=$( jhead "$global_file" 2>/dev/null )
    [[ "$info" ]] && {
        echo "<pre>$info"
        geo=$( echo "$info" | get_coords )
        [[ "$geo" ]] && {
            # google wants
            # https://www.google.com/maps/search/?api=1&query=47.5951518%2C-122.3316393
            echo -n '<a href="https://www.google.com/maps/search/?api=1&query='
            echo -n "$( urlencode "$geo")\">Google map</A>"
        }
        echo "</pre>"
    }
    echo "</td></tr></table>"

    output_trailer
}

# Function to generate HTML for a directory
output_dir_html() {
    output_normal_http_header
    desc=$( cat "$global_location/$title_file" 2> /dev/null)
    output_heading " - $global_location: $desc"
    pushd "$global_parent" > /dev/null || exit 1
    navigation_buttons
    popd > /dev/null || exit 1
    pushd "$global_location" > /dev/null|| exit 1
    echo "<p>"
    echo "<table><tr><td>"
    for thing in *; do
        [[ "$thing" =~ ($excl_dirs) ]] && continue
        if [[ -d "$thing" ]]; then
            desc=$( cat "$thing/$title_file" 2> /dev/null)
            if [[ "$HOSTNAME" == "aloo" ]]; then
                x=$( xxd -p < "$thing/$title_file" 2> /dev/null)
                if [[ "$x" =~ (53616c6c79|4572696b61) ]]; then
                    continue
                fi
            fi

            count="$( find "$thing" -maxdepth 1 -mindepth 1 -type f | grep -Ec "\\.($images|$videos)" )"
            if (( count > 0 )); then
                count=" ($count)"
            else
                count=""
            fi
            path="$global_location/$( urlencode "$thing" )"
            echo "<tr><td><a href=\"$global_uri?$path\">$thing</a></td><td><a href=\"$global_uri?$path\">$desc$count</a></td></tr>"
        else
            if [[ -f "$thing" ]]; then
                path="$( urlencode "$global_location/$thing" )"
                path="${path//%2f//}"
                if [[ "$thing" =~ \.($images)$ ]]; then
                    [[ "$thing" =~ \.$video_thumb$ ]] ||
                        echo "<a href=\"$global_uri?$path\"><img src=\"/$global_location/thumbnails/$thing\" height=\"$thumbnail_height\" alt=\"$thing\"></a>"
                elif [[ "$thing" =~ \.($videos)$ ]]; then
                    thumb="${thing%\.*}.$video_thumb"
                    echo "<a href=\"/$path\"><img src=\"/$global_location/thumbnails/$thumb\" height=\"$thumbnail_height\" alt=\"$thumb\"></a>"
                fi
            fi
        fi
    done
    echo "</td></tr></table>"
    output_trailer
}

output_scripts() {
    f=""
    up=""
    [[ "$fullscreen" ]] && {
        f="&fullscreen=1"
        up="/$global_file"
    }

    cat <<EOF
<script>
    // Swipe gesture support
    let startX = undefined, startY = undefined, lastTapTime = 0, isSwipe = false;

    document.addEventListener('touchstart', function(event) {
        if (document.activeElement.tagName !== 'INPUT') {
            const currentTime = new Date().getTime();
            const tapInterval = currentTime - lastTapTime;

            // Double-tap detection (within 300ms)
            if (tapInterval > 0 && tapInterval <= 300) {
                // Double-tap action
                window.location.href = '$global_uri${global_parent:+?${global_parent}}';
                lastTapTime = 0;
                return;
            }

            // Update last tap time
            lastTapTime = currentTime;

            // Record starting coordinates for swipe
            startX = event.touches[0].clientX;
            startY = event.touches[0].clientY;
            isSwipe = false;
        }
    });

    document.addEventListener('touchmove', function(event) {
        if (startX !== undefined && document.activeElement.tagName !== 'INPUT') {
            const moveX = event.touches[0].clientX - startX;
            const moveY = event.touches[0].clientY - startY;

            // Detect horizontal swipe
            if (Math.abs(moveX) > Math.abs(moveY) && Math.abs(moveX) > 20) {
                isSwipe = true;
                event.preventDefault(); // Prevent default only for horizontal swipe
            }
        }
    });

    document.addEventListener('touchend', function(event) {
        if (isSwipe && startX !== undefined && document.activeElement.tagName !== 'INPUT') {
            const endX = event.changedTouches[0].clientX;
EOF
    [[ "$global_next" ]] && cat << EOF
            // Swipe left
            if (startX - endX > 50) {
                window.location.href = '$global_uri?${global_parent:+${global_parent}/}$global_next$f';
            }
EOF
    [[ "$global_prev" ]] && cat << EOF
            // Swipe right
            if (endX - startX > 50) {
                window.location.href = '$global_uri?${global_parent:+${global_parent}/}$global_prev$f';
            }
EOF
    cat << EOF
        }
        startX = undefined;
        startY = undefined;
        isSwipe = false;
    });
EOF

    cat << EOF
    // Keyboard support:
    document.addEventListener('keydown', function(event) {
        if (document.activeElement.tagName === 'INPUT') return;
        let targetPath;
        switch (event.key)  {
            case 'ArrowUp':
                window.location.href = '$global_uri?$global_parent$up';
                return;
EOF
    [[ "$global_first" ]] && cat << EOF
            case 'Home':
                targetPath = '$global_first$f';
                break;
EOF
    [[ "$global_prev" ]] && cat << EOF
            case 'ArrowLeft':
                targetPath = '$global_prev$f';
                break;
EOF
    [[ "$global_next" ]] && cat << EOF
            case 'ArrowRight':
                targetPath = '$global_next$f';
                break;
EOF
    [[ "$global_last" ]] && cat << EOF
            case 'End':
                targetPath = '$global_last$f';
                break;
EOF
    cat << EOF
            default:
                return;
        }
        window.location.href = '$global_uri?${global_parent:+${global_parent}/}' + targetPath;
    });
EOF

    cat << EOF
</script>
EOF
}

output_top_level_html() {
    output_normal_http_header
    output_heading
    output_search_html

    echo "<hr><table>"
    for year in *; do
        [[ "$year" =~ ($excl_dirs) ]] && continue

        [[ -d "$year" ]] && {
            desc=$( cat "$year/$title_file" 2> /dev/null)
            echo "<tr><td><a href=\"$global_uri?$year\"> $year: </a></td><td><a href=\"$global_uri?$year\">$desc</a></td></tr>"
        }
    done
    echo "</table>"
    output_trailer
}

output_fullscreen_http_header() {
    # Output the HTTP header
    echo "Content-type: text/html"
    echo
    cat << EOF
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
<style>
    body, html {
        height: 100%;
        margin: 0;
        overflow: hidden;
        background-color: black;
    }

    img {
        width: 100%;
        height: 100%;
        object-fit: contain;
        cursor: zoom-in;
    }
</style>
EOF
    output_scripts
    cat << EOF
<script>
    document.addEventListener('DOMContentLoaded', function() {
        var img = document.querySelector('img');
        img.addEventListener('click', function() {
            window.location.href = '../$global_location';
        });
    });
</script>
</head>
<body>
EOF
}

output_normal_http_header() {
    # Output the HTTP header
    echo "Content-type: text/html"
    echo
    cat << EOF
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <title>Photo viewer</title>
    <style>
        body, html {
            margin: 0;
            padding: 0;
            overflow: auto;
            font-family: Arial, sans-serif;
            background-color: #f0f0f0; /* Soft gray background */
            background-image: url(/viewer/bg.gif);
            background-size: cover;
            background-repeat: no-repeat;
            background-attachment: fixed;
        }
        img.scaled {
            max-width: 100%;     /* Limit width to container */
            max-height: 75vh;
            width: auto;
            height: auto;
            display: block;      /* Center the image in its container */
            margin: auto;
        }
        .constrained-table {
            max-width: 100vw;  /* Constrain to viewport width */
            max-height: 100vh; /* Constrain to viewport height */
            margin: auto;
        }
         form.inline-form {
            display: inline-block;
        }
    </style>
</head>
<body>
EOF
}

output_heading() {
    sub_heading="$1"
    cat << EOF
    <h2><a href="$global_uri">Photo viewer</a>$sub_heading</h2>
EOF
}

output_trailer() {
    cat << EOF
<hr>
</BODY>
EOF
    output_scripts
    cat << EOF
<ADDRESS>
Copyright &copy; 2024-2025 Bob Hepple. All rights reserved. Version $TIME_STAMP
</ADDRESS>
</html>
EOF
}

# main()

# these shell variables are provided by the seb server:
global_uri="http://$SERVER_NAME:$SERVER_PORT$SCRIPT_NAME"
# global_location is everything after the '?' ... can be
#                               ie blank - the root
# .                                   ditto
# 2019                          ie a directory
# 2019/IMG01.jpg                ie a jpg file
# 2019/IMG01.jpg&fullscreen=1   ie a jpg file but viewed fullscreen without decorations

echo "query_string='$QUERY_STRING'" >&2
fullscreen=""
[[ $QUERY_STRING =~ .*\&fullscreen=1 ]] && fullscreen="${QUERY_STRING#*&}"

# We're initially in the cgi-bin directory (at least when run from
# thttpd - other's may need a fix?)
cd ..

if [[ -z "$QUERY_STRING" ]]; then
    output_top_level_html
else
    if [[ "$QUERY_STRING" =~ ^search=(.*)\&root=(.*) ]]; then
        exec_search "$global_uri" "${BASH_REMATCH[1]}" "${BASH_REMATCH[2]}"
    else
        global_location="${QUERY_STRING%%&*}"
        global_parent="${global_location%/*}"
        global_file="${global_location##*/}"
        [[ "$global_parent" == "$global_file" ]] && global_parent=""
        pushd "$global_parent" > /dev/null || exit 1
        get_adjacent "${global_file}"
        popd > /dev/null || exit 1

        if [[ "$fullscreen" ]]; then
            output_fullscreen_html
        elif [[ -d "${global_location}" ]]; then
            output_dir_html
        else
            output_file_html
        fi
    fi
fi

# sample auto-deploy in emacs:
# (defun bh/auto-deploy ()
#   "install it"
#   (interactive)
#   (shell-command (concat "cp " buffer-file-name " (concat (getenv "HOME") "/media/photos/cgi-bin/")))

# Local Variables:
# mode: shell-script
# eval: (add-hook 'after-save-hook 'bh/auto-deploy nil t)
# time-stamp-pattern: "4/TIME_STAMP=\"%:y%02m%02d.%02H%02M%02S\""
# eval: (add-hook 'before-save-hook 'time-stamp)
# End:
