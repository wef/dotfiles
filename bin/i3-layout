#! /usr/bin/env -S python -B
# https://gitlab.com/wef/dotfiles/-/blob/master/bin/i3-layout

TIME_STAMP="20241207.075905"

"""
    Contact info:
    bob dot hepple at gmail dot com

    Copyright (C) 2021-23 Bob Hepple

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; see the file COPYING. If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth
    Floor, Boston, MA 02110-1301 USA
"""

import json, argparse, sys, os

sys.dont_write_bytecode = True

# globals:
args = None

def usage():
    print(f"""Usage: {progName} [OPTIONS]

Prints the layout of containers for i3/sway
""")

def backtick(command):
    from subprocess import Popen, PIPE
    value = Popen(["bash", "-c", command], stdout=PIPE).communicate()[0].rstrip().decode("utf-8")
    return value

def print_tree(dict, indent, sep):
    app = None
    Xclass = ""
    try:
        app = dict['app_id']
    except:
        pass
    if app == None:
        try:
            app = dict['window_properties']['instance']
            Xclass = "'"+dict['window_properties']['class']+"'"
        except:
            pass

    # first time through here, I assume we're doing the root node:
    orient = ''
    if dict['orientation'] == 'vertical':
        orient = 'V'
    elif dict['orientation'] == 'horizontal':
        orient = 'H'

    if args.compact:
        if orient:
            print("%s[" % orient, end="")
        if dict['type'] == "con" and app:
            print(f"{sep}{app}", end="")
    else:
        print("%s%s %s %s '%s' '%.40s' %s" % (indent, orient, dict['id'], dict['type'], app, dict['name'], Xclass))

    sep = ""
    for i in dict['nodes']:
        print_tree(i, indent + "|", sep)
        sep = " "
    sep = ""
    for i in dict['floating_nodes']:
        print_tree(i, indent + "|", sep)
        sep = " "

    if args.compact and orient:
        print("]", end="")

if __name__ == '__main__':
    dirName, progName = os.path.split(sys.argv[0])
    msg = "i3-msg"
    if os.getenv("SWAYSOCK") != "":
        msg = "swaymsg"

    version = TIME_STAMP
    config_file = os.getenv("HOME")+"/.config/"+progName+"/config."+os.getenv("HOSTNAME")+".json"

    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=f"Print the container layout for i3/sway.\nVersion {version}",
                                     epilog="""
""")
    # parser.add_argument("-d", "--debug", help="increase output (presently NOP)", action="store_true")
    parser.add_argument("-w", "--workspace", help="print specified workspace only (0==current)", metavar="<workspace>", type = str)
    parser.add_argument("-c", "--compact", help="compact layout (does not print floaters). Same as .representation workspace field in sway.", action="store_true")
    args = parser.parse_args()

    if args.workspace:
        if args.workspace == "0":
            subtree = backtick(f"{msg} -t get_tree | jq '.. | select(.type? == \"workspace\" and (.. | select(.focused?)))'")
        else:
            subtree = backtick(f"{msg} -t get_tree | jq '.. | select(.type? == \"workspace\" and .num? == {args.workspace})'")
        if not subtree:
            print(f"{progName}: no such workspace (%s)" % args.workspace, file = sys.stderr)
            sys.exit(1)
        tree = json.loads(subtree)
    else:
        tree = json.loads(backtick(f"{msg} -t get_tree"))

    print_tree(tree, "", "")
    if args.compact:
        print("")

# Local variables:
# mode: python
# time-stamp-pattern: "4/TIME_STAMP=\"%:y%02m%02d.%02H%02M%02S\""
# eval: (add-hook 'before-save-hook 'time-stamp)
# End:
