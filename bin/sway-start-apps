#!/usr/bin/env bash
# https://gitlab.com/wef/dotfiles/-/blob/master/bin/sway-start-apps
# shellcheck disable=SC2034
TIME_STAMP="20250129.123442"

# Copyright (C) 2020-2024 Bob Hepple <bob dot hepple at gmail dot com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

PROG=$( basename "$0" )

[[ "$1" ]] && {
    echo "Usage: $PROG: system-specific auto-startup apps for sway"
    exit 0
}

start_apps() {
    
    echo "Building workspaces, please wait ..." | wofi --dmenu & wofi_pid=$!
    
    case $USER in
        bhepple)
            swaymsg -q 'workspace 1'
            if [[ "$OUTPUT" =~ HEADLESS-.* ]]; then
                i3-toolwait --nocheck -v 'myterm'
                swaymsg "output * bg /usr/share/backgrounds/sway/Sway_Wallpaper_Blue_1920x1080.png fill"
            else
                i3-toolwait -v --waitfor emacs logit emacs

                swaymsg -q 'workspace 3'
                i3-toolwait --nocheck -v -- myterm
                swaymsg -q 'splith'
                i3-toolwait --nocheck -v -- myterm
                
                swaymsg -q 'workspace 2'
                browser
                swaymsg -q 'border none'

                swaymsg -q 'workspace 4'
                i3-toolwait -v --waitfor pavucontrol -- logit pavucontrol
                swaymsg -q 'splitv'
            fi
            ;;
        
        dvr) # on the media server:
            /usr/libexec/xdg-desktop-portal &
            /usr/libexec/xdg-desktop-portal-wlr &
            /usr/libexec/xdg-desktop-portal-gtk &
            swaymsg -q 'workspace 2'
            i3-toolwait -v --waitfor Firefox firefox
            swaymsg -q 'workspace 3'
            i3-toolwait --nocheck -v 'myterm'
            swaymsg -q 'split h'
            i3-toolwait --nocheck -v 'myterm'
            swaymsg -q 'workspace 4'
            i3-toolwait -v --waitfor org.pulseaudio.pavucontrol 'pavucontrol'
            #swaymsg -q 'workspace 5'
            #i3-toolwait -v 'transmission-gtk'

            # do last as it's a beast:
            swaymsg -q 'workspace 1'
            i3-toolwait -v -- 'mythfrontend' -platform xcb
            wayvnc 0.0.0.0 5905 &
            ;;
        *)
            wayvnc 0.0.0.0 5906 &            
            ;;
    esac
}

# shellcheck disable=SC1091
source /etc/os-release

# FIXME: the following lines may need adjusting to your environment!

OUTPUT=$( swaymsg -t get_outputs| jq -r '.. | select(.type?) | select(.focused==true) | .name' )
echo "$PROG: OUTPUT=$OUTPUT"

# no point doing eval at this point - hopefully it's already set up in sway-start.
# gkd has all components by default, so no need to --start anything!
# gnome-keyring-daemon --start --components=secrets,ssh,pkcs11

# these startups are pretty universal:

logit sway-prep-xwayland
logit /usr/libexec/gsd-xsettings
logit dunst

logit wl-paste -t text --watch myclipman

logit sudo rfkill block bluetooth
logit bluetoothctl power off

# needed for blueman-manager
logit blueman-applet &
logit nm-applet --indicator &

if [[ "$ID" = "void" ]]; then 
    logit pipewire &
else # fedora
    sudo systemctl restart ydotool
    # /usr/libexec/polkit-gnome-authentication-agent-1 fails eg with firewall-config:
    logit lxpolkit &
fi

logit i3-track-window-events &
logit dark-mode res
logit bluetooth-timeout &

noapps="$HOME/noapps"
[[ -f "$noapps" ]] || start_apps
rm -f "$noapps"

case $( hostname ) in
    achar*)
        # see also /etc/zzz.d/resume/70-disable-wlp3s0
        nmcli device disconnect wlp3s0
        ;& # fall through
    dosa) # laptop
        logit start-powerd
        logit mylock --quiet safe-mode
        logit wlr-sunclock -a tblr -l bottom
        ;;
    aloo)
        :
        ;;
    roti)
        logit mylock --quiet desktop
        logit wlr-sunclock -a tblr -l bottom
        ;;
esac

kill "$wofi_pid"

# Local Variables:
# mode: shell-script
# time-stamp-pattern: "4/TIME_STAMP=\"%:y%02m%02d.%02H%02M%02S\""
# eval: (add-hook 'before-save-hook 'time-stamp)
# End:
