#!/usr/bin/env bash
# https://gitlab.com/wef/dotfiles/-/blob/master/bin/i3-set-layout
# shellcheck disable=SC2034
TIME_STAMP="20241207.091620"

# Copyright (C) 2024 Bob Hepple <bob dot hepple at gmail dot com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

do_focused_first() {
    # but only if it's in the list
    focused="$1"
    shift
    found=""
    for candidate in "$@"; do
        if [[ "$candidate" == "$focused" ]]; then
            found="set"
            break
        fi
    done
    if [[ "$found" ]]; then
        echo "$focused"
    fi
    
    while [[ "$1" ]]; do
        [[ "$1" == "$focused" ]] || echo "$1"
        shift
    done
}

do_msg() {
    echo "$msg $*"
    $msg -q "$*"
}

prog=$(basename "$0")
version="$TIME_STAMP"
max_grid=20

msg="i3-msg"
[[ "$SWAYSOCK" ]] && {
    msg="swaymsg"
}

dir=""
case "$1" in
    v*) dir="splitv" ;;
    h*) dir="splith" ;;
    m*) dir="monocle" ;;
    s*) dir="spiral" ;;
    g*)
        dir="grid"
        shift
        cols="$1"
        shift
        y="$1"
        (( cols <= 0 || cols > max_grid )) && {
            echo "$prog: arguments for grid are out of range (1-$max_grid)" >&2
            exit 1
        }
        ;;
    *)
        arguments="["
        usage=""
        echo "Usage: $prog v(ertical)|h(orizontal)|s(piral)|m(onocle)|g(rid) COLS"
        echo
        echo "Layout all tiled windows in the current workspace (for i3wm or sway)."
        echo "The focused window is positioned first."
        echo "With 'grid', COLS must be given and is the number of columns (1..$max_grid)"
        echo
        echo "WARNINGS:"
        echo "'grid' works on i3wm but fails on sway - see https://github.com/swaywm/sway/issues/8482"
        echo "BUG: 'grid's with >2 rows will be sized unequally"
        echo "full-screen windows and nested windows will confuse the script - keep it simple."
        echo
        echo "Suggested bindsyms:"
        echo "bindsym \$mod+Control+h exec i3-set-layout horizontal"
        echo "bindsym \$mod+Control+v exec i3-set-layout vertical"
        echo "bindsym \$mod+Control+m exec i3-set-layout monocle"
        echo "bindsym \$mod+Control+s exec i3-set-layout spiral"
        echo "bindsym \$mod+Control+2 exec i3-set-layout grid 2"
        echo "bindsym \$mod+Control+3 exec i3-set-layout grid 3"
        echo "bindsym \$mod+Control+4 exec i3-set-layout grid 4"
        exit 0
        ;;
esac

if [[ "$SWAYSOCK" ]]; then
    query='select(.type=="con")|select(.visible==true)|select(.app_id != null or .window_properties.instance != null)'
else
    query='select(.window_properties?) | select(.floating == "auto_off" or .floating == "user_off") | select(.output != "__i3")'
fi

# Set the layout for all windows in the focused workspace to vertical/horizontal
tree=$( $msg -t get_tree )
focused=$( echo "$tree" | jq ".. | select(.type?) | select(.focused==true).id" )
mapfile -t id_list < <( echo "$tree" | jq ".. | select(.type?) | $query | .id" )
[[ ${#id_list[@]} -lt 2 ]] && {
    echo "$prog: too few tiled windows in the current workspace" >&2
    exit 1
}

# NB we need to send everything off screen, change the layout and
# bring them back - otherwise we get nasty nested containers.

# shellcheck disable=SC2068
for id in ${id_list[@]}; do
    do_msg "[con_id=$id] fullscreen disable, move to workspace 20"
done

# focus on the workspace:
count=0
while true; do
    do_msg "focus parent"
    f=$( $msg -t get_tree |
         jq ".. | select(.type?) | select(.focused==true).type" )
    [[ "$f" == '"workspace"' ]] && break
    count=$(( count + 1 ))
    (( count > 10 )) && {
        echo "prog: can't find the workspace (too deeply nested?)" >&2
        exit 1
    }
done

case "$dir" in
    splitv) 
        do_msg "layout splitv"
        ;;
    *)
        do_msg "layout splith"
        ;;
esac

# shellcheck disable=SC2068
mapfile -t id_list < <( do_focused_first "$focused" ${id_list[@]} )

count=0
# shellcheck disable=SC2068
for id in ${id_list[@]}; do
    do_msg "[con_id=$id] move to workspace current, focus"
    i3-layout -cw 0
    case "$dir" in
        split*) : ;;
        m*)
            (( count > 0 )) && {
                do_msg "splitv"
                dir="splitv"
            }
            ;;
        s*)
            if (( count % 2 == 0 )); then
                do_msg "split h"
            else
                do_msg "split v"
            fi
            ;;
        g*)
            if (( ( count + 1 ) % cols == 0 )); then
                if (( count > 1 )); then
                    do_msg 'focus parent'
                fi
                do_msg 'focus parent, split v'
            elif (( ( count + 1 ) % cols == 1 )); then
                do_msg 'split h'
            fi
            ;;                    
    esac
    i3-layout -cw 0
    count=$(( count + 1 ))
done

[[ "$focused" ]] && do_msg "[con_id=$focused] focus"

# Local Variables:
# mode: shell-script
# time-stamp-pattern: "4/TIME_STAMP=\"%:y%02m%02d.%02H%02M%02S\""
# eval: (add-hook 'before-save-hook 'time-stamp)
# End:

