# bin

Some scripts that I use. The i3-* ones also apply to **sway**(1)

- **alarm**: called by battery-alarm to do the actual alarm with notify-send
- **argp.sh**: wrapper for **getopt**(3) for **bash**(1) scripts - used in many scripts here
- **battery-alarm**: called by power-monitor if battery is low
- **bluetooth-timeout**: turns bluetooth off if no connections 
- **ccd**: redefines the **cd**(1) command to use **fzf**(1)
- **check-links**: checks that all links to this git repo are in place
- **dark-mode**: toggles or refreshes dark-mode for **emacs**(1), any terminal eg **foot**(1), gtk and qt
- **fzf-launcher**: launcher that offers search on application category as well as name
- **fzf-xbps**: Simple TUI/fzf package manager for xbps-based systems like voidlinux
- **fs**: wrapper around rg with fallback to grep; fzf support
- **fuzzypkg**: slightly modified version of https://github.com/zdykstra/fuzzypkg - TUI for xbps package mgr
- **get-podcasts**: downloads mp3 podcasts from RSS feeds
- **i3-border-tweak**: tweak window borders avoiding csd (**kitty**(1) barfs on them).
- **i3-fit-floats**: arrange floating windows between visible and scratchpad 
- **i3-focus**: Give focus to a program based on app_id (wayland) or class (Xwayland)
- **i3-hide**: Hides the current terminal and runs the gui-program
- **i3-kill**: kills the focused program
- **i3-layout**: display the layout of i3 or sway windows
- **i3-menu**: popup a menu of sway/i3 commands
- **i3-mode**: displays some help for a sway 'mode' (uses nwg-wrapper)
- **i3-move-to**: move a window to top-right, bottom-right, etc
- **i3-next-empty-workspace**: Jumps to the next empty workspace.
- **i3-pip**: make the current window (or run a command) Picture-In-Picture
- **i3-print-containers**: prints out the layout of the **i3wm**(1) or **sway**(1) containers
- **i3-prop**: shows the properties of the focused window
- **i3-run-or-raise**: jump to a running program and focus it - else start it
- **i3-runner**: Run a swaymsg/i3-msg command on the Nth sway/i3 session, typically from a ssh tty or from **cron**(1) or **at**(1)
- **i3-set-layout**: Arrange the existing windows in a i3 or sway workspace to be horiz, vert, monocle, spiral, grid
- **i3-toolwait**: a python version of **toolwait**(1) - run a program and wait for a new window to appear
- **i3-track-window-events**: track and respond to window events
- **logit**: add datestamps to stdout and err
- **lookup**: will prompt for and save a password using **gpg2**(1).
- **low-power**: responds to charger dis/connect events from power-monitor eg reduces screen brightness
- **lswifi**: calls **nmcli**(1) and adds physical location data
- **myautotype**: Autotype using **xvkdb**(1)/**xdotool**(1)/**ydotool**(1)
- **mybluetooth**: popup a menu for bluetooth operations
- **myclipman**: keepassxc-safe wrapper for **clipman**(1)
- **mykp**: Wrapper for **keepass-cli**(1) - safe for **clipman**(1)
- **mylock**: Set up **swayidle**(1) with automatic screen blanking, lock and/or suspend after an idle period.
- **mypass**: Wrapper for **pass**(1) - safe for **clipman**(1)
- **myreload**: reload the **sway**(1) configuration, possibly with confirmation; also check if reload is necessary
- **myrofi**: Calls **rofi**(1) with my theme
- **myscreendump**: Take a screendump with a choice of window, frame, rectangle or root. wlroots or Xorg.
- **myterm**: Run a terminal
- **mywob**: pops up **wob**(1)
- **play-mythtv**: list all mythtv recording with **fzf**(1) and plays the one selected with **mpv**(1)
- **power-monitor**: monitors laptop battery and charger, raises alarms (python)
- **recentf**: prints files from ~/.local/share/recently-used.xbel (gnome database) - bash hence slower
- **recentf-etree**: prints files from ~/.local/share/recently-used.xbel (gnome database) - python using etree (no cleanup option)
- **recentf-gtk**: prints files from ~/.local/share/recently-used.xbel (gnome database) - python using gtk (no cleanup option)
- **singleton**: make sure there is one <program> running on _this_ session
- **sway-count**: graphical countdown
- **sway-prep-xwayland**: once **sway**(1) is running, load up xwayland's xrdb database, etc
- **sway-select-window**: show running programs and select one to display with **wofi**(1)
- **sway-select-window2**: show running programs and select one to display with **wofi**(1) (in **python**(1))
- **sway-start**: start a **sway**(1) session from a console tty
- **sway-start-apps**: system-specific auto-startup apps for **sway**(1)
- **sway-vnc**: run a VNC connection to a remote system, starting **wayvnc*(1) and/or **sway**(1) if necessary
- **toggle-devices**: display a bunch of buttons and take appropriate actions on pressing them
- **toggle-easyeffects**: run easyeffects if not running; else kill it
- **toolwait**: a bash version of **toolwait**(1) - run a program and wait for a new window to appear
- **xcheck**: run a GUI command capturing stdout and stderr (X11 or Wayland).

<!--
NB gfm mode in emacs seems to be borked - 100% cpu!!
Local Variables:
mode: text
markdown-command: "pandoc -s -f gfm --metadata title=README"
eval: (add-hook 'after-save-hook (lambda nil (shell-command (concat markdown-command " README.md > README.html"))) nil 'local)
End:
-->
